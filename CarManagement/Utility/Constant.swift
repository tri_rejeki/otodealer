//
//  Constant.swift
//  CarManagement
//
//  Created by Tri Rejeki on 01/12/17.
//  Copyright © 2017 OTOmart: Cara Mudah Cari Mobil. All rights reserved.
//

import Foundation

struct SEGUE {
    public static let HOME = "segueToHome"
    public static let LOCAL = "segueToLocal"
    public static let LIVE = "segueToLive"
    public static let FORM = "segueToForm"
    public static let IMAGE = "segueToImageDetail"
}

struct CarURL {
    static let KEY = "5AN08DI437598349539204SUR37YA2"
    static let BASE_URL = "http://54.169.247.8/webrestfull/"
    static let WEB_URL = "http://54.169.247.8/dealer/login.htm"
    static let LOGIN = "check_login_auth.json"
    static let ADD_CAR = "add_car.json"
    static let GET_COLOR = "get_color.json"
    static let GET_MERK = "get_merk.json"
    static let GET_MODEL = "get_model.json"
    static let GET_TYPE = "get_type.json"
    static let GET_CC = "get_cc.json"
    static let GET_MODEL_ID = "get_model_by_id.json"
    static let GET_TYPE_ID = "get_type_by_id.json"
    static let GET_CC_ID = "get_cc_by_id.json"
    static let GET_LOGIN_APP = "get_login_app.json"
    static let GET_MASTER_DATA = "get_master_data.json"
}

struct UserData {
    static let LOGIN = "login"
    static let LAST_UPDATE = "last_update"
}
