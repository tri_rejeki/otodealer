//
//  DataUtility.swift
//  CarManagement
//
//  Created by Tri Rejeki on 04/12/17.
//  Copyright © 2017 OTOmart: Cara Mudah Cari Mobil. All rights reserved.
//

import Foundation
import CoreData
import UIKit

public class DataUtility {
    
    public static var context: NSManagedObjectContext?
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //MARK: Fetch Function
    public static func fetchAllMerk() -> [Merk] {
        var fetchedResults: [Merk] = []
        
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            context = appDelegate.managedObjectContext
        }
        
        do {
            fetchedResults = (try context?.fetch(Merk.fetchRequest()))!
            print(fetchedResults)
        }
        catch {
            print ("fetch merk failed")
        }
        
        return fetchedResults
    }
    
    public static func fetchAllWarna() -> [Warna]{
        var fetchedResults: [Warna] = []
        
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            context = appDelegate.managedObjectContext
        }
        
        do {
            fetchedResults = (try context?.fetch(Warna.fetchRequest()))!
            print(fetchedResults)
        }
        catch {
            print ("fetch warna failed")
        }
        return fetchedResults
    }
    
    public static func fetchAllModel() -> [Model]{
        var fetchedResults: [Model] = []
        
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            context = appDelegate.managedObjectContext
        }
        
        do {
            fetchedResults = (try context?.fetch(Model.fetchRequest()))!
            print(fetchedResults)
        }
        catch {
            print ("fetch model failed")
        }
        return fetchedResults
    }
    
    public static func fetchAllType() -> [Type] {
        var fetchedResults: [Type] = []
        
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            context = appDelegate.managedObjectContext
        }
        
        do {
            fetchedResults = (try context?.fetch(Type.fetchRequest()))!
            print(fetchedResults)
        }
        catch {
            print ("fetch type failed")
        }
        return fetchedResults
    }
    
    public static func fetchAllCC() -> [Cc]{
        var fetchedResults: [Cc] = []
        
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            context = appDelegate.managedObjectContext
        }
        
        do {
            fetchedResults = (try context?.fetch(Cc.fetchRequest()))!
            print(fetchedResults)
        }
        catch {
            print ("fetch cc failed")
        }
        return fetchedResults
    }
    
    public static func fetchAllCarList() -> [CarList] {
        var fetchedResults: [CarList] = []
        
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            context = appDelegate.managedObjectContext
        }
        
        do {
            fetchedResults = (try context?.fetch(CarList.fetchRequest()))!
            print(fetchedResults)
        }
        catch {
            print ("fetch Car List failed")
        }
        return fetchedResults
    }
    
    //MARK: Fetch by ID
    
    public static func fetchModelByMerk(merkId:String) -> [Model]{
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            context = appDelegate.managedObjectContext
        }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Model")
        let predicate = NSPredicate(format: "merk_id == %@", merkId)
        fetchRequest.predicate = predicate
        let result = try? context?.fetch(fetchRequest)
        let resultData = result as! [Model]
        
        return resultData
    }
    
    public static func fetchTypeByModel(modelId:String) -> [Type]{
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            context = appDelegate.managedObjectContext
        }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Type")
        let predicate = NSPredicate(format: "model_id == \(modelId)")
        fetchRequest.predicate = predicate
        let result = try? context?.fetch(fetchRequest)
        let resultData = result as! [Type]
        
        return resultData
    }
    
    public static func fetchCCByTypeId(id:String) -> [Cc]{
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            context = appDelegate.managedObjectContext
        }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Cc")
        let predicate = NSPredicate(format: "id_type == \(id)")
        fetchRequest.predicate = predicate
        let result = try? context?.fetch(fetchRequest)
        let resultData = result as! [Cc]
        
        return resultData
    }
    
    //MARK: Fetch by personal ID
    
    public static func fetchWarna(id:String) -> Warna {
        var warna:Warna = Warna()
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            context = appDelegate.managedObjectContext
        }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Warna")
        let attendancePredicate = NSPredicate(format: "color_id == \(id)")
        fetchRequest.predicate = attendancePredicate
        
        print("color_id = \(id)")
        
        do {
            let result = try context?.fetch(fetchRequest)
            warna = result![0] as! Warna
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return warna
    }
    
    public static func fetchCc(id:String) -> Cc {
        var cc:Cc = Cc()
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            context = appDelegate.managedObjectContext
        }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Cc")
        let attendancePredicate = NSPredicate(format: "id_type == \(id)")
        fetchRequest.predicate = attendancePredicate
        
        print("id_type = \(id)")
        
        do {
            let result = try context?.fetch(fetchRequest)
            cc = result![0] as! Cc
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return cc
    }
    
    public static func fetchMerk(id:String) -> Merk {
        var merk:Merk = Merk()
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            context = appDelegate.managedObjectContext
        }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Merk")
        let attendancePredicate = NSPredicate(format: "merk_id == \(id)")
        fetchRequest.predicate = attendancePredicate
        
        print("merk_id = \(id)")
        
        do {
            let result = try context?.fetch(fetchRequest)
            merk = result![0] as! Merk
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return merk
    }
    
    public static func fetchType(id:String) -> Type {
        var type:Type = Type()
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            context = appDelegate.managedObjectContext
        }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Type")
        let attendancePredicate = NSPredicate(format: "id == \(id)")
        fetchRequest.predicate = attendancePredicate
        
        print("id = \(id)")
        
        do {
            let result = try context?.fetch(fetchRequest)
            type = result![0] as! Type
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return type
    }
    
    public static func fetchModel(id:String) -> Model {
        var model:Model = Model()
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            context = appDelegate.managedObjectContext
        }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Model")
        let attendancePredicate = NSPredicate(format: "id == \(id)")
        fetchRequest.predicate = attendancePredicate
        
        print("id = \(id)")
        
        do {
            let result = try context?.fetch(fetchRequest)
            model = result![0] as! Model
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return model
    }
    
    public static func fetchWarnaName(id:String) -> String? {
        var warna:Warna = Warna()
        var warnaStr:String?
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            context = appDelegate.managedObjectContext
        }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Warna")
        let attendancePredicate = NSPredicate(format: "id == \(id)")
        fetchRequest.predicate = attendancePredicate
        
        print("id = \(id)")
        
        do {
            let result = try context?.fetch(fetchRequest)
            warna = result![0] as! Warna
            warnaStr = warna.name!
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return warnaStr
    }
    
    public static func fetchCarList(id:String) -> CarList {
        var car:CarList = CarList()
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            context = appDelegate.managedObjectContext
        }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CarList")
        let attendancePredicate = NSPredicate(format: "id == \(id)")
        fetchRequest.predicate = attendancePredicate
        
        print("\(id)")
        
        do {
            let result = try context?.fetch(fetchRequest)
            car = result![0] as! CarList
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return car
    }
    
    //MARK: Insert Function
    
    public static func insertArrayOfWarna(datas:[Dictionary<String, AnyObject>]) {
        for obj in datas {
            if #available(iOS 10.0, *) {
                self.context = self.appDelegate.persistentContainer.viewContext
            } else {
                self.context = self.appDelegate.managedObjectContext
            }
            
            let warna = NSEntityDescription.insertNewObject(forEntityName: "Warna", into: self.context!) as! Warna
            warna.id = obj["id"] as? String ?? "0"
            warna.name = obj["name"] as? String ?? ""
            
            do {
                try self.context?.save()
                print("Warna Saved : \(warna)")
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    public static func insertArrayOfModel(datas:[Dictionary<String, AnyObject>]) {
        for obj in datas {
            if #available(iOS 10.0, *) {
                self.context = self.appDelegate.persistentContainer.viewContext
            } else {
                self.context = self.appDelegate.managedObjectContext
            }
            
            let model = NSEntityDescription.insertNewObject(forEntityName: "Model", into: self.context!) as! Model
            model.id = obj["id"] as? String ?? "0"
            model.name = obj["name"] as? String ?? ""
            model.merk_id = obj["merk_id"] as? String ?? "0"
            
            do {
                try self.context?.save()
                print("Model Saved : \(model)")
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    public static func insertArrayOfCc(datas:[Dictionary<String, AnyObject>]) {
        for obj in datas {
            if #available(iOS 10.0, *) {
                self.context = self.appDelegate.persistentContainer.viewContext
            } else {
                self.context = self.appDelegate.managedObjectContext
            }
            
            let cc = NSEntityDescription.insertNewObject(forEntityName: "Cc", into: self.context!) as! Cc
            cc.id_type = obj["id"] as? String ?? "0"
            cc.cc = obj["cc"] as? String ?? ""
            
            do {
                try self.context?.save()
                print("CC Saved : \(cc)")
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    public static func insertArrayOfType(datas:[Dictionary<String, AnyObject>]) {
        for obj in datas {
            if #available(iOS 10.0, *) {
                self.context = self.appDelegate.persistentContainer.viewContext
            } else {
                self.context = self.appDelegate.managedObjectContext
            }
            
            let type = NSEntityDescription.insertNewObject(forEntityName: "Type", into: self.context!) as! Type
            type.name = obj["name"] as? String ?? ""
            type.id = obj["id"] as? String ?? "0"
            type.model_id = obj["model_id"] as? String ?? "0"
            
            do {
                try self.context?.save()
                print("Type Saved : \(type)")
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    public static func insertArrayOfMerk(datas:[Dictionary<String, AnyObject>]) {
        for obj in datas {
            if #available(iOS 10.0, *) {
                self.context = self.appDelegate.persistentContainer.viewContext
            } else {
                self.context = self.appDelegate.managedObjectContext
            }
            
            let merk = NSEntityDescription.insertNewObject(forEntityName: "Merk", into: self.context!) as! Merk
            merk.name = obj["name"] as? String ?? ""
            merk.merk_id = obj["id"] as? String ?? "0"
            
            do {
                try self.context?.save()
                print("Merk Saved : \(merk)")
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    public static func insertCarData(data:Dictionary<String, AnyObject>) -> Bool {
        if #available(iOS 10.0, *) {
            self.context = self.appDelegate.persistentContainer.viewContext
        } else {
            self.context = self.appDelegate.managedObjectContext
        }
        
        let car = NSEntityDescription.insertNewObject(forEntityName: "CarList", into: self.context!) as! CarList
        car.color_id = data["color_id"] as? String ?? "0"
        car.model_id = data["model_id"] as? String ?? "0"
        car.id = data["id"] as? String ?? "0"
        car.condition = data["condition"] as? String ?? ""
        car.desc = data["desc"] as? String ?? ""
        car.end_stnk = data["end_stnk"] as? String ?? ""
        car.fuel = data["fuel"] as? String ?? ""
        car.id_kota = data["id_kota"] as? String ?? "0"
        car.id_provinsi = data["id_provinsi"] as? String ?? ""
        car.kilometer = data["kilometer"] as? String ?? ""
        car.merk_id = data["merk_id"] as? String ?? "0"
        car.otomart_id = data["otomart_id"] as? String ?? "0"
        car.picture = data["picture"] as? Data
        car.picture1 = data["picture1"] as? Data
        car.picture2 = data["picture2"] as? Data
        car.picture3 = data["picture3"] as? Data
        car.picture4 = data["picture4"] as? Data
        car.picture5 = data["picture5"] as? Data
        car.picture6 = data["picture6"] as? Data
        car.picture7 = data["picture7"] as? Data
        car.picture8 = data["picture8"] as? Data
        car.picture9 = data["picture9"] as? Data
        car.price = data["price"] as? String ?? ""
        car.size_cc = data["size_cc"] as? String ?? ""
        car.special_type = data["special_type"] as? String ?? ""
        car.transmition = data["transmition"] as? String ?? ""
        car.type_id = data["type_id"] as? String ?? "0"
        car.year = data["year"] as? String ?? ""
        car.no_polisi = data["no_polisi"] as? String ?? ""
        
        do {
            try self.context?.save()
            print("Car Saved : \(car)")
            return true
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
            return false
        }
    }
    
    //MARK: Update Data
    public static func UpdateCarList(id:String, data:Dictionary<String,AnyObject>) -> Bool {
        var car:CarList = CarList()
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            context = appDelegate.managedObjectContext
        }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CarList")
        let attendancePredicate = NSPredicate(format: "id == \(id)")
        fetchRequest.predicate = attendancePredicate
        
        let result = try? context?.fetch(fetchRequest)
        car = result!![0] as! CarList
        car.color_id = data["color_id"] as? String ?? ""
        car.model_id = data["model_id"] as? String ?? ""
        car.id = data["id"] as? String ?? ""
        car.condition = data["condition"] as? String ?? ""
        car.desc = data["desc"] as? String ?? ""
        car.end_stnk = data["end_stnk"] as? String ?? ""
        car.fuel = data["fuel"] as? String ?? ""
        car.id_kota = data["id_kota"] as? String ?? ""
        car.id_provinsi = data["id_provinsi"] as? String ?? ""
        car.kilometer = data["kilometer"] as? String ?? ""
        car.merk_id = data["merk_id"] as? String ?? ""
        car.otomart_id = data["otomart_id"] as? String ?? ""
        car.picture = data["picture"] as? Data
        car.picture1 = data["picture1"] as? Data
        car.picture2 = data["picture2"] as? Data
        car.picture3 = data["picture3"] as? Data
        car.picture4 = data["picture4"] as? Data
        car.picture5 = data["picture5"] as? Data
        car.picture6 = data["picture6"] as? Data
        car.picture7 = data["picture7"] as? Data
        car.picture8 = data["picture8"] as? Data
        car.picture9 = data["picture9"] as? Data
        car.price = data["price"] as? String ?? ""
        car.size_cc = data["size_cc"] as? String ?? ""
        car.special_type = data["special_type"] as? String ?? ""
        car.transmition = data["transmition"] as? String ?? ""
        car.type_id = data["type_id"] as? String ?? ""
        car.year = data["year"] as? String ?? ""
        car.no_polisi = data["no_polisi"] as? String ?? ""
        
        do {
            try self.context?.save()
            print("Car Updated : \(car)")
            return true
        } catch let error as NSError {
            print("Could not update \(error), \(error.userInfo)")
            return false
        }
    }
    
    //MARK : Delete Core Data Field
    public static func deleteAllData(entity: String){
        if #available(iOS 10.0, *) {
            context = self.appDelegate.persistentContainer.viewContext
        } else {
            context = self.appDelegate.managedObjectContext
        }
        
        let delAllReqVar = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: entity))
        do {
            try context?.execute(delAllReqVar)
        }
        catch {
            print(error)
        }
    }
    
    public static func deleteCarDataById(carId:String) {
        if #available(iOS 10.0, *) {
            context = self.appDelegate.persistentContainer.viewContext
        } else {
            context = self.appDelegate.managedObjectContext
        }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CarList")
        let attendancePredicate = NSPredicate(format: "id == \(carId)")
        fetchRequest.predicate = attendancePredicate
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try context?.execute(deleteRequest)
        }
        catch {
            print(error)
        }
    }
}
