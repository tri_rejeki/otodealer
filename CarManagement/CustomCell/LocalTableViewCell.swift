//
//  LocalTableViewCell.swift
//  CarManagement
//
//  Created by Tri Rejeki on 02/12/17.
//  Copyright © 2017 OTOmart: Cara Mudah Cari Mobil. All rights reserved.
//

import UIKit

protocol LocalDataDelegate {
    func didSelecEdit(data:CarList)
    func didCarSelected(data:CarList,isOn:Bool)
}

class LocalTableViewCell: UITableViewCell {

    @IBOutlet weak var pilih:UISwitch!
    @IBOutlet weak var merek:UILabel!
    @IBOutlet weak var model:UILabel!
    @IBOutlet weak var edit:UIButton!
    
    var carData:CarList?
    var delegate: LocalDataDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func didTapEditCar(_ sender: Any) {
        delegate?.didSelecEdit(data: carData!)
    }
    
    @IBAction func carSelected(_ sender: Any) {
        delegate?.didCarSelected(data: carData!,isOn:pilih.isOn)
    }
}
