//
//  ImageTableViewCell.swift
//  CarManagement
//
//  Created by Tri Rejeki on 15/12/17.
//  Copyright © 2017 OTOmart: Cara Mudah Cari Mobil. All rights reserved.
//

import UIKit
import CarouselSwift

class ImageTableViewCell: UITableViewCell {

    @IBOutlet weak var carouselImage: CarouselView!
    @IBOutlet weak var pagging: UIPageControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
