//
//  FormFieldTableViewCell.swift
//  CarManagement
//
//  Created by Tri Rejeki on 09/12/17.
//  Copyright © 2017 OTOmart: Cara Mudah Cari Mobil. All rights reserved.
//

import UIKit

class FormFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var titleField: UILabel!
    @IBOutlet weak var valueTextField: UITextField!
    @IBOutlet weak var dropDown: UIImageView!
    @IBOutlet weak var notesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
