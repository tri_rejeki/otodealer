//
//  FormSwitchTableViewCell.swift
//  CarManagement
//
//  Created by Tri Rejeki on 09/12/17.
//  Copyright © 2017 OTOmart: Cara Mudah Cari Mobil. All rights reserved.
//

import UIKit

protocol FormSwitchDelegate {
    func returnSwitchValue(result:String)
}

class FormSwitchTableViewCell: UITableViewCell {

    @IBOutlet weak var titleField: UILabel!
    @IBOutlet weak var switchField: UISwitch!
    
    var delegate:FormSwitchDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func doChangeSwitch(_ sender: Any) {
        let switchResult = switchField.isOn
        if switchResult {
            delegate?.returnSwitchValue(result: "true")
        }else{
            delegate?.returnSwitchValue(result: "false")
        }
    }
}
