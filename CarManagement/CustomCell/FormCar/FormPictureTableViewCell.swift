//
//  FormPictureTableViewCell.swift
//  CarManagement
//
//  Created by Tri Rejeki on 09/12/17.
//  Copyright © 2017 OTOmart: Cara Mudah Cari Mobil. All rights reserved.
//

import UIKit

protocol FormTapImageDelegate {
    func returnTapImage1()
    func returnTapImage2()
    func returnTapImage3()
    func returnTapImage4()
    func returnTapImage5()
    func returnTapImage6()
    func returnTapImage7()
    func returnTapImage8()
    func returnTapImage9()
    func returnTapImage10()
    func returnPrevImage(index:Int)
}

class FormPictureTableViewCell: UITableViewCell {

    @IBOutlet weak var frontImage: UIImageView!
    @IBOutlet weak var frontButton: UIButton!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var rightImage: UIImageView!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var leftImage: UIImageView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var interiorImage: UIImageView!
    @IBOutlet weak var interiorButton: UIButton!
    @IBOutlet weak var sixImage: UIImageView!
    @IBOutlet weak var sixButton: UIButton!
    @IBOutlet weak var sevenImage: UIImageView!
    @IBOutlet weak var sevenButton: UIButton!
    @IBOutlet weak var eightImage: UIImageView!
    @IBOutlet weak var eightButton: UIButton!
    @IBOutlet weak var nineImage: UIImageView!
    @IBOutlet weak var nineButton: UIButton!
    @IBOutlet weak var tenImage: UIImageView!
    @IBOutlet weak var tenButton: UIButton!
    
    var formDelegate:FormTapImageDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tapFront = UITapGestureRecognizer(target: self, action: #selector(frontImageSelected(_:)))
        frontImage.isUserInteractionEnabled = true
        frontImage.addGestureRecognizer(tapFront)
        
        let tapBack = UITapGestureRecognizer(target: self, action: #selector(backImageSelected(_:)))
        backImage.isUserInteractionEnabled = true
        backImage.addGestureRecognizer(tapBack)
        
        let tapRight = UITapGestureRecognizer(target: self, action: #selector(rightImageSelected(_:)))
        rightImage.isUserInteractionEnabled = true
        rightImage.addGestureRecognizer(tapRight)
        
        let tapLeft = UITapGestureRecognizer(target: self, action: #selector(leftImageSelected(_:)))
        leftImage.isUserInteractionEnabled = true
        leftImage.addGestureRecognizer(tapLeft)
        
        let tapInterior = UITapGestureRecognizer(target: self, action: #selector(interiorImageSelected(_:)))
        interiorImage.isUserInteractionEnabled = true
        interiorImage.addGestureRecognizer(tapInterior)
        
        let tapSix = UITapGestureRecognizer(target: self, action: #selector(sixImageSelected(_:)))
        sixImage.isUserInteractionEnabled = true
        sixImage.addGestureRecognizer(tapSix)
        
        let tapSeven = UITapGestureRecognizer(target: self, action: #selector(sevenImageSelected(_:)))
        sevenImage.isUserInteractionEnabled = true
        sevenImage.addGestureRecognizer(tapSeven)
        
        let tapEight = UITapGestureRecognizer(target: self, action: #selector(eightImageSelected(_:)))
        eightImage.isUserInteractionEnabled = true
        eightImage.addGestureRecognizer(tapEight)
        
        let tapNine = UITapGestureRecognizer(target: self, action: #selector(nineImageSelected(_:)))
        nineImage.isUserInteractionEnabled = true
        nineImage.addGestureRecognizer(tapNine)
        
        let tapTen = UITapGestureRecognizer(target: self, action: #selector(tenImageSelected(_:)))
        tenImage.isUserInteractionEnabled = true
        tenImage.addGestureRecognizer(tapTen)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func frontImageSelected(_ sender:AnyObject){
        formDelegate?.returnTapImage1()
    }
    
    @objc func backImageSelected(_ sender:AnyObject){
        formDelegate?.returnTapImage2()
    }
    
    @objc func rightImageSelected(_ sender:AnyObject){
        formDelegate?.returnTapImage3()
    }
    
    @objc func leftImageSelected(_ sender:AnyObject){
        formDelegate?.returnTapImage4()
    }
    
    @objc func interiorImageSelected(_ sender:AnyObject){
        formDelegate?.returnTapImage5()
    }
    
    @objc func sixImageSelected(_ sender:AnyObject){
        formDelegate?.returnTapImage6()
    }
    
    @objc func sevenImageSelected(_ sender:AnyObject){
        formDelegate?.returnTapImage7()
    }
    
    @objc func eightImageSelected(_ sender:AnyObject){
        formDelegate?.returnTapImage8()
    }
    
    @objc func nineImageSelected(_ sender:AnyObject){
        formDelegate?.returnTapImage9()
    }
    
    @objc func tenImageSelected(_ sender:AnyObject){
        formDelegate?.returnTapImage10()
    }

    @IBAction func deleteFrontImage(_ sender: Any) {
        formDelegate?.returnPrevImage(index: 1)
    }
    
    @IBAction func deleteBackImage(_ sender: Any) {
        formDelegate?.returnPrevImage(index: 2)
    }
    
    @IBAction func deleteRightImage(_ sender: Any) {
        formDelegate?.returnPrevImage(index: 3)
    }
    
    @IBAction func deleteLeftImage(_ sender: Any) {
        formDelegate?.returnPrevImage(index: 4)
    }
    
    @IBAction func deleteInteriorImage(_ sender: Any) {
        formDelegate?.returnPrevImage(index: 5)
    }
    
    @IBAction func deleteSixImage(_ sender: Any) {
        formDelegate?.returnPrevImage(index: 6)
    }
    
    @IBAction func deleteSevenImage(_ sender: Any) {
        formDelegate?.returnPrevImage(index: 7)
    }
    
    @IBAction func deleteEightImage(_ sender: Any) {
        formDelegate?.returnPrevImage(index: 8)
    }
    
    @IBAction func deleteNineImage(_ sender: Any) {
        formDelegate?.returnPrevImage(index: 9)
    }
    
    @IBAction func deleteTenImage(_ sender: Any) {
        formDelegate?.returnPrevImage(index: 10)
    }
}
