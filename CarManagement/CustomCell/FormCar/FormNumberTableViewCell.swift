//
//  FormNumberTableViewCell.swift
//  
//
//  Created by Tri Rejeki on 23/12/17.
//

import UIKit

class FormNumberTableViewCell: UITableViewCell {

    @IBOutlet weak var titleField: UILabel!
    @IBOutlet weak var valueTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
