//
//  DashboardViewController.swift
//  CarManagement
//
//  Created by Tri Rejeki on 01/12/17.
//  Copyright © 2017 OTOmart: Cara Mudah Cari Mobil. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
import NVActivityIndicatorView

class DashboardViewController: UIViewController, NVActivityIndicatorViewable {
    
    @IBOutlet weak var txtLastUpdate: UILabel!
    @IBOutlet weak var btnLiveData: UIButton!
    @IBOutlet weak var btnLocalData: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var txtHelloDealer: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    var loginData:Login?
    var dateLastUpdate: String = ""
    let userDefault = UserDefaults.standard
    var context: NSManagedObjectContext?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            context = appDelegate.persistentContainer.viewContext
        } else {
            
            context = appDelegate.managedObjectContext
        }
        txtLastUpdate.text = dateLastUpdate
        self.setupNavTitle()
        self.checkLastUpdate()
        self.setupLoader()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: Setup
    func setupNavTitle(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "background_dashboard"), for: .default)
        self.navigationItem.title = "Car Management"
        
        txtHelloDealer.text = "Halo \(loginData!.name)"
    }
    
    func setupLoader() {
        NVActivityIndicatorView.DEFAULT_TYPE = .ballSpinFadeLoader
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
        NVActivityIndicatorView.DEFAULT_TEXT_COLOR = UIColor.white
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor.darkGray
        NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE_FONT = UIFont.boldSystemFont(ofSize: 14)
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Loading")
    }
    
    //MARK: Function
    func checkLastUpdate() {
        if let lastupdate  = userDefault.object(forKey: UserData.LAST_UPDATE) as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy 'at' h:mm a"
            let finalDate = dateFormatter.date(from:lastupdate)!
            let date = Calendar.current.date(byAdding: .month, value: 1, to: finalDate)
            let currentDate = Date()
            
            if (currentDate > date!){
                self.showLatesUpdate()
            }else{
                self.txtLastUpdate.text = lastupdate
            }
        }else{
            DataUtility.deleteAllData(entity: "Merk")
            DataUtility.deleteAllData(entity: "Type")
            DataUtility.deleteAllData(entity: "Model")
            DataUtility.deleteAllData(entity: "Cc")
            DataUtility.deleteAllData(entity: "Warna")
            
            self.loadAPI()
        }
    }
    
    func saveLastUpdate() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy 'at' h:mm a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        let lastupdate = dateFormatter.string(from: Date())
        self.txtLastUpdate.text = lastupdate
        UserDefaults.standard.set(lastupdate, forKey: UserData.LAST_UPDATE)
    }
    
    func showLatesUpdate() {
        let alert = UIAlertController(title: "OTOMART", message: "Tersedia master data baru untuk aplikasi. update sekarang?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Iya", style: UIAlertActionStyle.default, handler: doTapYes))
        alert.addAction(UIAlertAction(title: "Tidak", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertUpdate() {
        let alert = UIAlertController(title: "OTOMART", message: "Apakah anda akan update sekarang?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Iya", style: UIAlertActionStyle.default, handler: doTapYes))
        alert.addAction(UIAlertAction(title: "Tidak", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func doTapYes(alert: UIAlertAction!) {
        let size = CGSize(width: 50, height: 50)
        startAnimating(size, message: "Updating Data...")
        
        DataUtility.deleteAllData(entity: "Merk")
        DataUtility.deleteAllData(entity: "Type")
        DataUtility.deleteAllData(entity: "Model")
        DataUtility.deleteAllData(entity: "Cc")
        DataUtility.deleteAllData(entity: "Warna")
        
        self.loadAPI()
    }
    
    //MARK: API Request
    func loadAPI() {
        let size = CGSize(width: 50, height: 50)
        startAnimating(size, message: "Updating Data...")
        self.getWarna()
    }
    
    func getWarna() {
        let url = CarURL.BASE_URL+CarURL.GET_COLOR
        
        let param = [
            "key": CarURL.KEY
        ]
        
        Alamofire.request(url, method: .get, parameters: param).responseJSON { response in
            switch response.result {
            case .success:
                if var value = response.result.value as? [Dictionary<String, AnyObject>]{
                    var newWarna:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
                    newWarna.updateValue("0" as AnyObject, forKey: "id")
                    newWarna.updateValue("Lainnya" as AnyObject, forKey: "name")
                    value.append(newWarna)
                    DataUtility.insertArrayOfWarna(datas: value)
                    self.getCc()
                }
                break
            case .failure:
                break
            }
        }
    }
    
    func getCc() {
        let url = CarURL.BASE_URL+CarURL.GET_CC
        
        let param = [
            "key": CarURL.KEY
        ]
        
        Alamofire.request(url, method: .get, parameters: param).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value as? [Dictionary<String, AnyObject>]{
                    DataUtility.insertArrayOfCc(datas: value)
                    self.getMerk()
                }
                break
            case .failure:
                break
            }
        }
    }
    
    func getMerk() {
        let url = CarURL.BASE_URL+CarURL.GET_MERK
        
        let param = [
            "key": CarURL.KEY
        ]
        
        Alamofire.request(url, method: .get, parameters: param).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value as? [Dictionary<String, AnyObject>]{
                    DataUtility.insertArrayOfMerk(datas: value)
                    self.getType()
                }
                break
            case .failure:
                break
            }
        }
    }
    
    func getType() {
        let url = CarURL.BASE_URL+CarURL.GET_TYPE
        
        let param = [
            "key": CarURL.KEY
        ]
        
        Alamofire.request(url, method: .get, parameters: param).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value as? [Dictionary<String, AnyObject>]{
                    DataUtility.insertArrayOfType(datas: value)
                    self.getModel()
                }
                break
            case .failure:
                break
            }
        }
    }
    
    func getModel() {
        let url = CarURL.BASE_URL+CarURL.GET_MODEL
        
        let param = [
            "key": CarURL.KEY
        ]
        
        Alamofire.request(url, method: .get, parameters: param).responseJSON { response in
            self.stopAnimating()
            switch response.result {
            case .success:
                if let value = response.result.value as? [Dictionary<String, AnyObject>]{
                    DataUtility.insertArrayOfModel(datas: value)
                    self.saveLastUpdate()
                }
                break
            case .failure:
                break
            }
        }
    }
    
    //MARK: Action Button
    @IBAction func didTapLocalData(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE.LOCAL, sender: self)
    }
    
    @IBAction func didTapLiveData(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE.LIVE, sender: self)
    }
    
    @IBAction func didTapUpdateData(_ sender: Any) {
        self.showAlertUpdate()
    }
    
    @IBAction func didTapLogout(_ sender: Any) {
        self.showAlertLogout()
    }
    
    func showAlertLogout() {
        let alert = UIAlertController(title: nil, message: "Apakah anda yakin akan logout sekarang?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Iya", style: UIAlertActionStyle.default, handler: doTapYesLogout))
        alert.addAction(UIAlertAction(title: "Tidak", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func doTapYesLogout(alert: UIAlertAction!) {
        DataUtility.deleteAllData(entity: "Merk")
        DataUtility.deleteAllData(entity: "Type")
        DataUtility.deleteAllData(entity: "Model")
        DataUtility.deleteAllData(entity: "Cc")
        DataUtility.deleteAllData(entity: "Warna")
        DataUtility.deleteAllData(entity: "CarList")
        
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
        defaults.synchronize()
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE.LOCAL {
            let vc = segue.destination as! LocalDataViewController
            vc.loginData = self.loginData
        }
    }
}
