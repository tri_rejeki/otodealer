//
//  LocalDataViewController.swift
//  CarManagement
//
//  Created by Tri Rejeki on 01/12/17.
//  Copyright © 2017 OTOmart: Cara Mudah Cari Mobil. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SwiftMessages

class LocalDataViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var searchBar:UISearchBar!
    @IBOutlet weak var tableMobil:UITableView!
    @IBOutlet weak var btnTambah: UIButton!
    @IBOutlet weak var swtichAll : UISwitch!
    @IBOutlet weak var txtHelloDealer: UILabel!
    @IBOutlet weak var btnUpload: UIButton!
    
    var loginData:Login?
    var carArr:[CarList] = []
    var carSearch:[CarList] = []
    var carSelected:[CarList] = []
    var carUploaded:[CarList] = []
    var selectedCar:CarList?
    var isSelectAll:Bool = false
    var isAdd:Bool = false
    var isSearching:Bool = false
    var isUploadFailed:Bool = false
    let size = CGSize(width: 50, height: 50)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavTitle()
        searchBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupData()
        self.setupLoader()
        self.carUploaded = []
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: Setup
    func setupNavTitle(){
        self.navigationItem.title = "List Mobil Lokal"
        txtHelloDealer.text = "Halo \(loginData!.name)"
    }
    
    func setupData(){
        carArr = DataUtility.fetchAllCarList()
        tableMobil.reloadData()
    }
    
    func setupLoader() {
        NVActivityIndicatorView.DEFAULT_TYPE = .ballSpinFadeLoader
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
        NVActivityIndicatorView.DEFAULT_TEXT_COLOR = UIColor.white
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor.darkGray
        NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE_FONT = UIFont.boldSystemFont(ofSize: 14)
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Loading")
    }
    
    //MARK: Function
    func showError(message:String) {
        let error = MessageView.viewFromNib(layout: .cardView)
        error.configureTheme(.error)
        error.configureContent(title: "", body: message)
        error.button?.isHidden = true
        SwiftMessages.show(view: error)
    }
    
    func showSuccess(message:String) {
        let msg = MessageView.viewFromNib(layout: .cardView)
        msg.configureTheme(.success)
        msg.configureContent(title: "", body: message)
        msg.button?.isHidden = true
        SwiftMessages.show(view: msg)
    }
    
    func removeFormatAmount(string:String) -> NSNumber{
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyAccounting
        formatter.currencySymbol = "Rp."
        formatter.decimalSeparator = ","
        return formatter.number(from: string) ?? 0
    }
    
    //MARK: Action Button
    @IBAction func didTapAddCar(_ sender: Any) {
        isAdd = true
        self.performSegue(withIdentifier: SEGUE.FORM, sender: self)
    }
    
    @IBAction func didTapUploadData(_ sender: Any) {
        self.btnUpload.isEnabled = false
        if carSelected.count == 0 {
            self.showError(message: "Pilih data yang akan di upload.")
        }else{
            if carSelected.count > 1 {
                self.doMultipleUpload()
            }else{
                self.doSingleUpload()
            }
        }
    }
    
    func doSingleUpload() {
        isUploadFailed = false
        if Reachability.isConnectedToNetwork() {
            self.startAnimating(size, message: "Uploading...")
            self.uploadFile(data: carSelected[0])
        }else{
            self.showError(message: "Anda tidak terhubung dengan internet.")
        }
    }
    
    func doMultipleUpload() {
        isUploadFailed = false
        self.startAnimating(size, message: "Uploading...")
        if Reachability.isConnectedToNetwork() {
            if carSelected.count > 0 {
                for obj in carSelected {
                    if isUploadFailed == false {
                        self.uploadFile(data: obj)
                    }else{
                        self.stopAnimating()
                        self.carSelected = []
                        self.carUploaded = []
                        self.tableMobil.reloadData()
                        break
                    }
                }
            }else{
                self.stopAnimating()
                self.showError(message: "Pilih data yang akan di upload.")
            }
        }else{
            self.stopAnimating()
            self.showError(message: "Anda tidak terhubung dengan internet.")
        }
    }
    
    @IBAction func isSelectedAll(_ sender: Any) {
        isSelectAll = !isSelectAll
        if isSelectAll {
            carSelected = []
            carSelected = carArr
        }else{
            carSelected = []
        }
        tableMobil.reloadData()
    }
    
    
    //MARK: API Integration
    func uploadFile(data:CarList) {
        var fuelId = 4
        if data.fuel == "Gasoline" {
            fuelId = 1
        }else if data.fuel == "Diesel" {
            fuelId = 2
        }else if data.fuel == "Hybrid" {
            fuelId = 3
        }
        
        var endStnk:Date?
        if let endDate = data.end_stnk {
            if endDate != "" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                endStnk = dateFormatter.date(from: endDate)!
            }
        }
        
        var priceStr = data.price!.replacingOccurrences(of: ".", with: "")
        priceStr = priceStr.replacingOccurrences(of: "Rp", with: "")
        
        let param:[String : Any] = [
            "dealer_id": loginData!.id,
            "merk_id": data.merk_id!,
            "model_id": data.model_id!,
            "type_id": data.type_id!,
            "province_id": data.id_provinsi!,
            "city_id": data.id_kota!,
            "special_type": data.special_type ?? "",
            "description": data.desc ?? "",
            "price": priceStr,
            "year": data.year!,
            "transmition": (data.transmition == "AT") ? "1" : "2",
            "conditions": (data.condition == "Mobil Bekas") ? "1" : "2",
            "color_id": data.color_id!,
            "fuel":"\(fuelId)",
            "cc":data.size_cc!,
            "kilometer":data.kilometer!,
            "oto_staff_id" : data.otomart_id ?? "",
            "end_stnk": endStnk ?? "",
            "nopol":data.no_polisi ?? "",
            "guid":UUID().uuidString,
            "key":CarURL.KEY
        ]
        
        var imageParam:[String : Data] = [
            "picture":data.picture!,
            "picture1":data.picture1!,
            "picture2":data.picture2!,
            "picture3":data.picture3!,
            "picture4":data.picture4!
        ]
        
        if data.picture5 != nil {
            imageParam.updateValue(data.picture5!, forKey: "picture5")
        }
        
        if data.picture6 != nil {
            imageParam.updateValue(data.picture6!, forKey: "picture6")
        }
        
        if data.picture7 != nil {
            imageParam.updateValue(data.picture7!, forKey: "picture7")
        }
        
        if data.picture8 != nil {
            imageParam.updateValue(data.picture8!, forKey: "picture8")
        }
        
        if data.picture9 != nil {
            imageParam.updateValue(data.picture9!, forKey: "picture9")
        }
        
        let url = CarURL.BASE_URL+CarURL.ADD_CAR
        var request = URLRequest.init(url: URL(string: url)!)
        request.httpMethod = "POST"
        
        let boundary = self.generateBoundaryString()
        request.setValue( "multipart/form-data;boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        let body = NSMutableData()
        
        // Amazon S3 (probably others) wont take parameters after files, so we put them first
        for (key, value) in param {
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        
        for (key, value) in imageParam {
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(key).jpg\"\r\n")
            body.appendString("Content-Type: image/jpeg\r\n\r\n")
            body.append(value)
            body.appendString("\r\n")
        }
        
        body.appendString("--\(boundary)--\r\n")
        
        request.httpBody = body as Data
        
        if Reachability.isConnectedToNetwork() {
            let task = URLSession.shared.dataTask(with: request) {(dt, response, error) in
                if error != nil {
                    DispatchQueue.main.async {
                        self.btnUpload.isEnabled = true
                        self.stopAnimating()
                        self.showError(message: "Upload data gagal. Silahkan coba beberapa saat lagi.")
                        print(error ?? "")
                        self.isUploadFailed = true
                        if self.carUploaded.count != self.carSelected.count {
                            self.isSelectAll = false
                            self.swtichAll.isOn = self.isSelectAll
                            self.carSelected = []
                            self.carUploaded = []
                            self.tableMobil.reloadData()
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.btnUpload.isEnabled = true
                        self.stopAnimating()
                        print(response ?? "")
                        print("car id:\(data.id ?? "0")")
                        self.carUploaded.append(data)
                        if self.carUploaded.count == self.carSelected.count {
                            self.showSuccess(message: "Upload data success.")
                            self.isSelectAll = false
                            self.swtichAll.isOn = self.isSelectAll
                            self.carSelected = []
                            self.carUploaded = []
                        }
                        DataUtility.deleteCarDataById(carId: data.id ?? "0")
                        self.carArr = []
                        self.carArr = DataUtility.fetchAllCarList()
                        self.tableMobil.reloadData()
                    }
                }
            }
            task.resume()
        }else{
            self.stopAnimating()
            self.showError(message: "Anda tidak terhubung dengan internet.")
        }
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE.FORM {
            let vc = segue.destination as! FormCarViewController
            if let selCar = selectedCar {
                vc.carId = selCar.id!
                vc.isAdd = self.isAdd
            }
        }
        
        if segue.identifier == SEGUE.IMAGE {
            let vc = segue.destination as! ImageViewController
            if let selCar = selectedCar {
                vc.carData = selCar
            }
        }
    }
}

extension LocalDataViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (isSearching == true) ? carSearch.count : carArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: LocalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LocalTableViewCell", for: indexPath) as! LocalTableViewCell
        cell.carData = (isSearching == true) ? carSearch[indexPath.row] : carArr[indexPath.row]
        cell.delegate = self
        let merk = DataUtility.fetchMerk(id: cell.carData!.merk_id!).name
        let model = DataUtility.fetchModel(id: cell.carData!.model_id!).name
        cell.merek.text = merk
        cell.model.text = model
        
        cell.pilih.setOn(false, animated: true)
        for obj in carSelected {
            if obj.id == carArr[indexPath.row].id {
                cell.pilih.setOn(true, animated: true)
                break
            }else{
                cell.pilih.setOn(false, animated: true)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCar = (isSearching == true) ? carSearch[indexPath.row] : carArr[indexPath.row]
        self.performSegue(withIdentifier: SEGUE.IMAGE, sender: self)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
}

extension LocalDataViewController: LocalDataDelegate {
    func didSelecEdit(data: CarList) {
        isAdd = false
        selectedCar = data
        self.performSegue(withIdentifier: SEGUE.FORM, sender: self)
    }
    
    func didCarSelected(data: CarList, isOn: Bool) {
        if isOn {
            carSelected.append(data)
        }else{
            self.isSelectAll = false
            self.swtichAll.setOn(false, animated: true)
            for index in 0...carSelected.count-1 {
                if carSelected[index].id == data.id {
                    carSelected.remove(at: index)
                    break
                }
            }
        }
        self.tableMobil.reloadData()
    }
}

extension NSMutableData {
    
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

extension LocalDataViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.returnOriginalResult()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.showResultSearch(text: searchBar.text!)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.showResultSearch(text: searchBar.text!)
    }
    
    func showResultSearch(text: String) {
        isSearching = true
        self.carSearch = self.carArr.filter({ (data) -> Bool in
            let name = DataUtility.fetchModel(id: data.model_id!).name!.uppercased()
            let searchText:NSString = name as NSString
            return (searchText.range(of: text.uppercased(), options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        
        if text == ""{
            isSearching = false
        }
        
        self.tableMobil.reloadData()
    }
    
    func returnOriginalResult() {
        isSearching = false
    }
    
}
