//
//  LiveDataViewController.swift
//  CarManagement
//
//  Created by Tri Rejeki on 02/12/17.
//  Copyright © 2017 OTOmart: Cara Mudah Cari Mobil. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SwiftMessages

class LiveDataViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var webView: UIWebView!
    
    var loginData:Login?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLoader()
        self.navigationItem.title = "List Mobil Live"
        
        let size = CGSize(width: 50, height: 50)
        self.startAnimating(size, message: "Loading...")
        webView.delegate = self
        
        let userDefault = UserDefaults.standard
        if let logindata  = userDefault.object(forKey: UserData.LOGIN) as? Data {
            loginData = NSKeyedUnarchiver.unarchiveObject(with: logindata) as? Login
            let url: String = CarURL.BASE_URL + CarURL.GET_LOGIN_APP
            let requestURL = URL(string:url)
            let request : NSMutableURLRequest = NSMutableURLRequest(url: requestURL!)
            request.httpMethod = "POST"
            let parameters = "key=\(CarURL.KEY)&email=\(loginData!.email)&password=\(loginData!.password)"
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            webView.loadRequest(request as URLRequest)
        } else {
            let requestURL = URL(string:CarURL.WEB_URL)
            let request = URLRequest(url: requestURL!)
            webView.loadRequest(request)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupLoader() {
        NVActivityIndicatorView.DEFAULT_TYPE = .ballSpinFadeLoader
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
        NVActivityIndicatorView.DEFAULT_TEXT_COLOR = UIColor.white
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor.darkGray
        NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE_FONT = UIFont.boldSystemFont(ofSize: 14)
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Loading")
    }
}

extension LiveDataViewController: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.stopAnimating()
        self.showError(message: "Oops, something wrong.")
    }
    
    func showError(message:String) {
        let error = MessageView.viewFromNib(layout: .cardView)
        error.configureTheme(.error)
        error.configureContent(title: "", body: message)
        error.button?.isHidden = true
        SwiftMessages.show(view: error)
    }
}
