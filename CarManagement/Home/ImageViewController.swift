//
//  ImageViewController.swift
//  CarManagement
//
//  Created by Tri Rejeki on 15/12/17.
//  Copyright © 2017 OTOmart: Cara Mudah Cari Mobil. All rights reserved.
//

import UIKit
import CarouselSwift

class ImageViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var carData:CarList?
    
    var titleArr:[String] = ["Merk","Model", "Tipe", "Keterangan Tambahan", "Deskripsi", "Harga", "Tahun", "Transmisi", "Kondisi", "Warna", "Bahan Bakar", "Kapasitas Mesin", "Kilometer", "STNK Kendaraan", "Tanggal validasi STNK", "No Polisi","OTO Staff ID"]
    var images:[UIImage] = []
    var value:[String] = ["","", "", "", "", "", "", "", "", "", "", "", "", "false", "", "",""]
    var currentPage = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setNavigationTitle()
    }
    
    func setNavigationTitle() {
        self.navigationItem.title = "Car Data"
    }
    
    func setupData() {
        value[0] = DataUtility.fetchMerk(id: (carData?.merk_id!)!).name!
        value[1] = DataUtility.fetchModel(id: (carData?.model_id)!).name!
        value[2] = DataUtility.fetchType(id: (carData?.type_id)!).name!
        value[3] = carData?.special_type ?? "-"
        value[4] = carData?.desc ?? "-"
        value[5] = carData?.price ?? "-"
        value[6] = carData?.year ?? "-"
        value[7] = carData?.transmition ?? "-"
        value[8] = carData?.condition ?? "-"
        print(carData?.color_id ?? "0")
        if let warna:String = DataUtility.fetchWarnaName(id: (carData?.color_id)!) {
            value[9] = warna
        }else{
            value[9] = ""
        }
        value[10] = carData?.fuel ?? "-"
        value[11] = carData?.size_cc ?? "-"
        value[12] = carData?.kilometer ?? "-"
        if let isSTNK = carData?.end_stnk{
            value[13] = (isSTNK == "") ? "false" : "true"
            value[14] = isSTNK
        }else{
            value[13] = "false"
            value[14] = "-"
        }
        value[15] = carData?.no_polisi ?? "-"
        value[16] = carData?.otomart_id ?? "-"
        
        if let pictureData = carData?.picture {
            if let picture = UIImage(data: pictureData){
                images.append(picture)
            }
        }
        
        if let pictureData1 = carData?.picture1 {
            if let picture = UIImage(data: pictureData1){
                images.append(picture)
            }
        }
        
        if let pictureData2 = carData?.picture2 {
            if let picture = UIImage(data: pictureData2){
                images.append(picture)
            }
        }
        
        if let pictureData3 = carData?.picture3 {
            if let picture = UIImage(data: pictureData3){
                images.append(picture)
            }
        }
        
        if let pictureData4 = carData?.picture4 {
            if let picture = UIImage(data: pictureData4){
                images.append(picture)
            }
        }
        
        if let pictureData5 = carData?.picture5 {
            if let picture = UIImage(data: pictureData5){
                images.append(picture)
            }
        }
        
        if let pictureData6 = carData?.picture6 {
            if let picture = UIImage(data: pictureData6){
                images.append(picture)
            }
        }
        
        if let pictureData7 = carData?.picture7 {
            if let picture = UIImage(data: pictureData7){
                images.append(picture)
            }
        }
        
        if let pictureData8 = carData?.picture8 {
            if let picture = UIImage(data: pictureData8){
                images.append(picture)
            }
        }
        
        if let pictureData9 = carData?.picture9 {
            if let picture = UIImage(data: pictureData9){
                images.append(picture)
            }
        }

        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ImageViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell:ImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ImageTableViewCell", for: indexPath) as! ImageTableViewCell
            cell.carouselImage.delegate = self
            cell.carouselImage.type = .loop
            cell.carouselImage.pagingType = .cellLimit
            cell.carouselImage.cellPerPage = 1
            cell.carouselImage.dataSource = self
            cell.carouselImage.reload()
            cell.pagging.numberOfPages = images.count
            cell.pagging.currentPage = currentPage
            return cell
        case 5:
            let cell:TextViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TextViewTableViewCell", for: indexPath) as! TextViewTableViewCell
            cell.detailLabel.isEditable = false
            cell.detailLabel.text = value[indexPath.row-1]
            cell.titleLabel.text = titleArr[indexPath.row-1]
            return cell
        case 5:
            let cell:TextViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TextViewTableViewCell", for: indexPath) as! TextViewTableViewCell
            cell.detailLabel.isEditable = false
            cell.detailLabel.text = "Rp\(Int(value[indexPath.row-1])?.formattedWithSeparator ?? "")"
            cell.titleLabel.text = titleArr[indexPath.row-1]
            return cell
        case 14:
            let cell:SwitchTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SwitchTableViewCell", for: indexPath) as! SwitchTableViewCell
            cell.titleField.text = titleArr[indexPath.row-1]
            if value[indexPath.row-1] == "false" {
                cell.switchField.isOn = false
            }else{
                cell.switchField.isOn = true
            }
            cell.switchField.isEnabled = false
            return cell
        default:
            let cell:FieldTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FieldTableViewCell", for: indexPath) as! FieldTableViewCell
            cell.valueLabel.text = value[indexPath.row-1]
            cell.titleLabel.text = titleArr[indexPath.row-1]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 17
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 240.0
        case 5:
            return 150.0
        default:
            return 50.0
        }
    }
}

extension ImageViewController: CarouselViewDelegate, CarouselViewDataSourse {
    func carousel(_ carousel: CarouselView, didTapAt cell: Int) {
        
    }
    
    func carousel(_ carousel: CarouselView, didScrollFrom from: Int, to: Int) {
        currentPage = to
        self.tableView.reloadData()
    }
    
    func numberOfView(_ carousel: CarouselView) -> Int {
        return images.count
    }
    
    func carousel(_ carousel:CarouselView, viewForIndex index:Int) -> UIView? {
        let view:UIView = UIView(frame: carousel.frame)
        view.backgroundColor = UIColor.black
        
        let imageView:UIImageView = UIImageView(frame: carousel.frame)
        imageView.image = images[index]
        imageView.contentMode = .scaleAspectFit
        view.addSubview(imageView)
        
        return view
    }
}
