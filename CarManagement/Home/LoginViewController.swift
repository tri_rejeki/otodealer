//
//  LoginViewController.swift
//  CarManagement
//
//  Created by Tri Rejeki on 01/12/17.
//  Copyright © 2017 OTOmart: Cara Mudah Cari Mobil. All rights reserved.
//

import UIKit
import SwiftMessages
import NVActivityIndicatorView
import Alamofire

class LoginViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var viewBg: UIView!
    
    let userDefault = UserDefaults.standard
    var originalViewFrame:CGRect!
    var isUp:Bool = false
    var loginData:Login?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        txtUsername.text = "test-mobile-dev2@logique.co.id"
//        txtPassword.text = "test123"
        self.setupScroll()
        self.setupText()
        self.setupGesture()
        self.setupLoader()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupKeyboard()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let logindata  = userDefault.object(forKey: UserData.LOGIN) as? Data {
            loginData = NSKeyedUnarchiver.unarchiveObject(with: logindata) as? Login
            self.performSegue(withIdentifier: SEGUE.HOME, sender: self)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func keyboardWillShow(_ notification:Notification) {
        if !isUp {
            moveTextView(notification, up: true)
        }
    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
        if isUp {
            moveTextView(notification, up: false)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE.HOME {
            let destinationNavigationController = segue.destination as! UINavigationController
            let vc:DashboardViewController = destinationNavigationController.topViewController as! DashboardViewController
            vc.loginData = self.loginData
        }
    }
    
    //MARK: Setup
    func setupScroll() {
        scrollView.isScrollEnabled = true
        scrollView.backgroundColor = UIColor(patternImage: UIImage(named: "background_login")!)
        originalViewFrame = scrollView.frame
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
    }
    
    func setupText() {
        self.txtPassword.delegate = self
        self.txtUsername.delegate = self
    }
    
    func setupLoader() {
        NVActivityIndicatorView.DEFAULT_TYPE = .ballSpinFadeLoader
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
        NVActivityIndicatorView.DEFAULT_TEXT_COLOR = UIColor.white
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor.darkGray
        NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE_FONT = UIFont.boldSystemFont(ofSize: 14)
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Loading")
    }
    
    func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func setupGesture(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissTap(_:)))
        viewBg.addGestureRecognizer(tap)
        viewBg.isUserInteractionEnabled = true
    }
    
    //MARK: Function
    func isValidEmail(_ testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        
        return result
    }
    
    func moveTextView(_ notification: Notification, up:Bool) {
        isUp = up
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        var keyboardRect:CGRect!
        
        let animationDuration = (userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as AnyObject).doubleValue
        keyboardRect = (userInfo.object(forKey: UIKeyboardFrameEndUserInfoKey) as AnyObject).cgRectValue
        keyboardRect = self.view.convert(keyboardRect, from: nil)
        
        UIView.beginAnimations("ResizeForKeyboard", context: nil)
        UIView.setAnimationDuration(animationDuration!)
        
        if up {
            let keyboardTop:CGFloat = keyboardRect.origin.y
            var newTextViewFrame:CGRect = scrollView.frame
            newTextViewFrame.size.height = keyboardTop - scrollView.frame.origin.y
            scrollView.frame = newTextViewFrame
        } else {
            scrollView.frame = originalViewFrame
        }
        UIView.commitAnimations()
    }
    
    func showError(message:String) {
        let error = MessageView.viewFromNib(layout: .cardView)
        error.configureTheme(.error)
        error.configureContent(title: "", body: message)
        error.button?.isHidden = true
        SwiftMessages.show(view: error)
    }
    
    func showSuccess(message:String) {
        let error = MessageView.viewFromNib(layout: .cardView)
        error.configureTheme(.success)
        error.configureContent(title: "", body: message)
        error.button?.isHidden = true
        SwiftMessages.show(view: error)
    }
    
    //MARK: ActionButton
    
    @objc func dismissTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func doTapLogin(_ sender: Any) {
        self.view.endEditing(true)
        let email = txtUsername.text
        let password = txtPassword.text
        
        self.btnLogin.isEnabled = false
        
        if Reachability.isConnectedToNetwork() {
            if (email != "") {
                if isValidEmail(email!){
                    
                } else {
                    showError(message:"Email Anda tidak valid")
                    txtUsername.becomeFirstResponder()
                    self.btnLogin.isEnabled = true
                    return
                }
                
            } else {
                showError(message:"Email harus diisi")
                txtUsername.becomeFirstResponder()
                self.btnLogin.isEnabled = true
                return
            }
            
            if (password == ""){
                showError(message:"Password harus diisi")
                txtPassword.becomeFirstResponder()
                self.btnLogin.isEnabled = true
                return
            }
            
            let size = CGSize(width: 50, height: 50)
            startAnimating(size, message: "Loading...")
            
            let param = [
                "key": CarURL.KEY,
                "email": txtUsername.text!,
                "password": txtPassword.text!
            ]
            
            let url = CarURL.BASE_URL+CarURL.LOGIN
            
            Alamofire.request(url, method: .post, parameters: param).responseJSON { response in
                self.stopAnimating()
                switch response.result {
                case .success:
                    self.btnLogin.isEnabled = true
                    if let value = response.result.value as? Dictionary<String, AnyObject>{
                        self.showSuccess(message:"Login sukses.")
                        let cityId = (value["city_id"] is NSNull) ? "" : value["city_id"] as! String
                        let address = (value["address"] is NSNull) ? "" : value["address"] as! String
                        let dealerNew = (value["dealer_new"] is NSNull) ? "" : value["dealer_new"] as! String
                        let dealerUsed = (value["dealer_used"] is NSNull) ? "" : value["dealer_used"] as! String
                        let desc = (value["description"] is NSNull) ? "" : value["description"] as! String
                        let disctrictId = (value["distric_id"] is NSNull) ? "" : value["distric_id"] as! String
                        let email = (value["email"] is NSNull) ? "" : value["email"] as! String
                        let fax = (value["fax"] is NSNull) ? "" : value["fax"] as! String
                        let id = (value["id"] is NSNull) ? "" : value["id"] as! String
                        let idListing = (value["id_listing"] is NSNull) ? "" : value["id_listing"] as! String
                        let name = (value["name"] is NSNull) ? "" : value["name"] as! String
                        let phone = (value["phone"] is NSNull) ? "" : value["phone"] as! String
                        let phone1 = (value["phone1"] is NSNull) ? "" : value["phone1"] as! String
                        let phone2 = (value["phone2"] is NSNull) ? "" : value["phone2"] as! String
                        let picture = (value["picture"] is NSNull) ? "" : value["picture"] as! String
                        let provinceId = (value["province_id"] is NSNull) ? "" : value["province_id"] as! String
                        let latlong = (value["latlong"] is NSNull) ? "" : value["latlong"] as! String
                        let suspended = (value["suspended"] is NSNull) ? "" : value["suspended"] as! String
                        let url = (value["url"] is NSNull) ? "" : value["url"] as! String
                        let login = Login.init(cityId: cityId, address: address, dealerNew: dealerNew, dealerUsed: dealerUsed, desc: desc, disctrictId: disctrictId, email: email, fax: fax, id: id, idListing: idListing, name: name, phone: phone, phone1: phone1, phone2: phone2, picture: picture, provinceId: provinceId, suspended: suspended, url: url, latlong: latlong, password: self.txtPassword.text!)
                        
                        let logindata: Data = NSKeyedArchiver.archivedData(withRootObject: login as Login)
                        UserDefaults.standard.set(logindata, forKey: UserData.LOGIN)
                        self.loginData = NSKeyedUnarchiver.unarchiveObject(with: logindata) as? Login
                        self.txtUsername.text = ""
                        self.txtPassword.text = ""
                        self.performSegue(withIdentifier: SEGUE.HOME, sender: self)
                    }else{
                        self.btnLogin.isEnabled = true
                        self.showError(message:"Login gagal. Periksa email dan password Anda.")
                        self.txtUsername.text = ""
                        self.txtPassword.text = ""
                    }
                case .failure(let error):
                    self.btnLogin.isEnabled = true
                    self.showError(message:"Login gagal. Periksa email dan password Anda.")
                    self.txtUsername.text = ""
                    self.txtPassword.text = ""
                    print(error)
                }
            }
        }else{
            self.btnLogin.isEnabled = true
            self.showError(message:"Anda sedang offline. Cobalah beberapa saat lagi.")
        }
    }

}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == txtUsername){
            txtPassword.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return false
    }
}
