//
//  FormCarViewController.swift
//  CarManagement
//
//  Created by Tri Rejeki on 02/12/17.
//  Copyright © 2017 OTOmart: Cara Mudah Cari Mobil. All rights reserved.
//

import UIKit
import APJTextPickerView
import SwiftMessages
import NVActivityIndicatorView

class FormCarViewController: UIViewController, NVActivityIndicatorViewable {
    @IBOutlet weak var tableView: UITableView!
    
    var isAdd:Bool = true
    var carId: String = ""
    var isUp:Bool = false
    var originalViewFrame:CGRect!
    var carData:CarList?
    var loginData:Login?
    let userDefault = UserDefaults.standard
    
    //MARK: ARRAY DATA
    var carArr:[CarList] = []
    var merkArr:[Merk] = []
    var modelArr:[Model] = []
    var typeArr:[Type] = []
    var transmisi:[String] = ["MT", "AT"]
    var kondisi:[String] = ["Mobil Bekas"]//, "Mobil Baru"]
    var bahanBakar:[String] = ["Gasoline","Diesel","Hybrid","Lainnya"]
    var colorArr:[Warna] = []
    var ccArr:[Cc] = []
    var yearArr:[String] = []
    
    var titleArrayField = ["Merk","Model", "Tipe", "Keterangan Tambahan", "Deskripsi", "Harga", "Tahun", "Transmisi", "Kondisi", "Warna", "Bahan Bakar", "Kapasitas Mesin", "Kilometer", "STNK Kendaraan", "Tanggal validasi STNK", "No Polisi","OTO Staff ID"]
    var valueArrayField = ["","", "", "", "", "", "", "", "", "", "", "", "", "false", "", "",""]
    var images:[UIImage] = [UIImage(named:"icon_small_photo")!,UIImage(named:"icon_small_photo")!,UIImage(named:"icon_small_photo")!,UIImage(named:"icon_small_photo")!,UIImage(named:"icon_small_photo")!,UIImage(named:"icon_small_photo")!,UIImage(named:"icon_small_photo")!,UIImage(named:"icon_small_photo")!,UIImage(named:"icon_small_photo")!,UIImage(named:"icon_small_photo")!]
    var deleteImage:[Bool] = [false,false,false,false,false,false,false,false,false,false]
    let optionPictMessage: String = "Pilih foto tampak depan mobil terlebih dahulu pada bagian exterior"
    
    //MARK: ID's data
    var merkId = ""
    var modelId = ""
    var typeId = ""
    var colorId = ""
    var ccId = ""
    
    //MARK: Image Picker
    let imagePicker1 = UIImagePickerController()
    let imagePicker2 = UIImagePickerController()
    let imagePicker3 = UIImagePickerController()
    let imagePicker4 = UIImagePickerController()
    let imagePicker5 = UIImagePickerController()
    let imagePicker6 = UIImagePickerController()
    let imagePicker7 = UIImagePickerController()
    let imagePicker8 = UIImagePickerController()
    let imagePicker9 = UIImagePickerController()
    let imagePicker10 = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavTitle()
        self.setupKeyboard()
        self.setupLoader()
        if !isAdd {
            self.setupData()
        }
        
        originalViewFrame = tableView.frame
        
        merkArr = DataUtility.fetchAllMerk()
        colorArr = DataUtility.fetchAllWarna()
        
        if let logindata = userDefault.object(forKey: UserData.LOGIN) as? Data {
            loginData = NSKeyedUnarchiver.unarchiveObject(with: logindata) as? Login
        }
        
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        
        var i = Int(year)
        while i > 1899 {
            yearArr.append("\(i)")
            i -= 1
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tableView.frame = self.view.frame
    }
    
    @objc func keyboardWillShow(_ notification:Notification) {
        if !isUp {
            moveTextView(notification, up: true)
        }
    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
        if isUp {
            moveTextView(notification, up: false)
        }
    }
    
    func moveTextView(_ notification: Notification, up:Bool) {
        isUp = up
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        var keyboardRect:CGRect!
        
        let animationDuration = (userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as AnyObject).doubleValue
        keyboardRect = (userInfo.object(forKey: UIKeyboardFrameEndUserInfoKey) as AnyObject).cgRectValue
        keyboardRect = self.view.convert(keyboardRect, from: nil)
        
        UIView.beginAnimations("ResizeForKeyboard", context: nil)
        UIView.setAnimationDuration(animationDuration!)
        
        if up {
            let keyboardTop:CGFloat = keyboardRect.origin.y
            var newTextViewFrame:CGRect = tableView.frame
            newTextViewFrame.size.height = keyboardTop - tableView.frame.origin.y
            tableView.frame = newTextViewFrame
        } else {
            tableView.frame = originalViewFrame
        }
        UIView.commitAnimations()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: Setup
    func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(FormCarViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FormCarViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func setupNavTitle(){
        if isAdd {
            self.navigationItem.title = "Tambah Data Lokal"
        }else{
            self.navigationItem.title = "Edit Data Lokal"
        }
    }
    
    func setupLoader() {
        NVActivityIndicatorView.DEFAULT_TYPE = .ballSpinFadeLoader
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
        NVActivityIndicatorView.DEFAULT_TEXT_COLOR = UIColor.white
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor.darkGray
        NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE_FONT = UIFont.boldSystemFont(ofSize: 14)
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Loading")
    }
    
    func setupData() {
        carData = DataUtility.fetchCarList(id: carId)
        
        valueArrayField[0] = DataUtility.fetchMerk(id: (carData?.merk_id!)!).name!
        valueArrayField[1] = DataUtility.fetchModel(id: (carData?.model_id)!).name!
        valueArrayField[2] = DataUtility.fetchType(id: (carData?.type_id)!).name!
        valueArrayField[3] = carData?.special_type ?? ""
        valueArrayField[4] = carData?.desc ?? ""
        valueArrayField[5] = carData?.price ?? ""
        valueArrayField[6] = carData?.year ?? ""
        valueArrayField[7] = carData?.transmition ?? ""
        valueArrayField[8] = carData?.condition ?? ""
        
        if let idcolor:String = carData?.color_id {
            if let warna:String = DataUtility.fetchWarnaName(id: idcolor) {
                valueArrayField[9] = warna
            }else{
                valueArrayField[9] = ""
            }
        }
        
        valueArrayField[10] = carData?.fuel ?? ""
        valueArrayField[11] = carData?.size_cc ?? ""
        valueArrayField[12] = carData?.kilometer ?? ""
        
        valueArrayField[14] = carData?.end_stnk ?? ""
        
        if valueArrayField[14] == "" {
            valueArrayField[13] = "false"
        }else{
            valueArrayField[13] = "true"
        }
        
        valueArrayField[15] = carData?.no_polisi ?? ""
        valueArrayField[16] = carData?.otomart_id ?? ""
        
        merkId = (carData?.merk_id!)!
        modelArr = DataUtility.fetchModelByMerk(merkId: merkId)
        let tmpModelArr = DataUtility.fetchModelByMerk(merkId: "0")
        modelArr.append(contentsOf: tmpModelArr)
        modelId = (carData?.model_id!)!
        colorId = (carData?.color_id)!
        typeArr = DataUtility.fetchTypeByModel(modelId: modelId)
        let tmpTypeArr = DataUtility.fetchTypeByModel(modelId: "0")
        typeArr.append(contentsOf: tmpTypeArr)
        typeId = (carData?.type_id!)!
        ccArr = DataUtility.fetchCCByTypeId(id: typeId)
        
        if let pictureData = carData?.picture {
            if let picture = UIImage(data: pictureData){
                images[0] = picture
                deleteImage[0] = true
            }
        }
        
        if let pictureData1 = carData?.picture1 {
            if let picture = UIImage(data: pictureData1){
                images[1] = picture
                deleteImage[1] = true
            }
        }
        
        if let pictureData2 = carData?.picture2 {
            if let picture = UIImage(data: pictureData2){
                images[2] = picture
                deleteImage[2] = true
            }
        }
        
        if let pictureData3 = carData?.picture3 {
            if let picture = UIImage(data: pictureData3){
                images[3] = picture
                deleteImage[3] = true
            }
        }
        
        if let pictureData4 = carData?.picture4 {
            if let picture = UIImage(data: pictureData4){
                images[4] = picture
                deleteImage[4] = true
            }
        }
        
        if let pictureData5 = carData?.picture5 {
            if let picture = UIImage(data: pictureData5){
                images[5] = picture
                deleteImage[5] = true
            }
        }
        
        if let pictureData6 = carData?.picture6 {
            if let picture = UIImage(data: pictureData6){
                images[6] = picture
                deleteImage[6] = true
            }
        }
        
        if let pictureData7 = carData?.picture7 {
            if let picture = UIImage(data: pictureData7){
                images[7] = picture
                deleteImage[7] = true
            }
        }
        
        if let pictureData8 = carData?.picture8 {
            if let picture = UIImage(data: pictureData8){
                images[8] = picture
                deleteImage[8] = true
            }
        }
        
        if let pictureData9 = carData?.picture9 {
            if let picture = UIImage(data: pictureData9){
                images[9] = picture
                deleteImage[9] = true
            }
        }
        self.tableView.reloadData()
    }
    
    //MARK: Function
    
    @IBAction func didTapSave(_ sender: Any) {
        var continueToSave = true
        let valueSTNK = valueArrayField[13]
        for index in 0...valueArrayField.count-1 {
            if valueArrayField[index] == "" {
                if index != 16 {
                    if typeId == "0" {
                        if index == 14 {
                            if valueSTNK == "true"{
                                self.showError(message: "Anda belum mengisi Tanggal validasi STNK.")
                                continueToSave = false
                                break
                            }
                        }else if index == 15 {
                            if valueSTNK == "true"{
                                self.showError(message: "Anda belum mengisi No Polisi.")
                                continueToSave = false
                                break
                            }
                        }else{
                            self.showError(message: "Anda belum mengisi \(titleArrayField[index]).")
                            continueToSave = false
                            break
                        }
                    }else{
                        if index != 3 {
                            if index == 14 {
                                if valueSTNK == "true"{
                                    self.showError(message: "Anda belum mengisi Tanggal validasi STNK.")
                                    continueToSave = false
                                    break
                                }
                            }else if index == 15 {
                                self.showError(message: "Anda belum mengisi No Polisi.")
                                continueToSave = false
                                break
                            }else{
                                self.showError(message: "Anda belum mengisi \(titleArrayField[index]).")
                                continueToSave = false
                                break
                            }
                        }
                        if index == 15 {
                            self.showError(message: "Anda belum mengisi No Polisi.")
                            continueToSave = false
                            break
                        }
                    }
                }
            }
        }
        
        if continueToSave {
            for index in 0...4 {
                if images[index] == UIImage(named:"icon_small_photo") {
                    self.showError(message: "Anda belum melengkapi foto.")
                    continueToSave = false
                    break
                }
            }
        }
        
        if continueToSave {
            let size = CGSize(width: 50, height: 50)
            startAnimating(size, message: "Saving Data...")
            if isAdd {
                self.doSaveCarList()
            }else{
                self.doUpdateCarList()
            }
        }
    }
    
    func showError(message:String) {
        let error = MessageView.viewFromNib(layout: .cardView)
        error.configureTheme(.error)
        error.configureContent(title: "", body: message)
        error.button?.isHidden = true
        SwiftMessages.show(view: error)
    }
    
    func showSuccess(message:String) {
        let msg = MessageView.viewFromNib(layout: .cardView)
        msg.configureTheme(.success)
        msg.configureContent(title: "", body: message)
        msg.button?.isHidden = true
        SwiftMessages.show(view: msg)
    }
    
    func randomString(length: Int, letters: NSString) -> String {
        
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    func extractData() -> Dictionary<String, AnyObject> {
        carArr = DataUtility.fetchAllCarList()
        
        let randomNum:UInt32 = arc4random_uniform(100)
        let someInt:Int = Int(randomNum)
        var letters:NSString = "\(someInt)\(merkId)\(modelId)\(typeId)" as NSString
        letters = letters.replacingOccurrences(of: " ", with: "") as NSString
        let idCar = self.randomString(length: 11, letters: letters)
        var data:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
        data.updateValue(colorId as AnyObject, forKey: "color_id")
        data.updateValue(merkId as AnyObject, forKey: "merk_id")
        data.updateValue(modelId as AnyObject, forKey: "model_id")
        data.updateValue(typeId as AnyObject, forKey: "type_id")
        data.updateValue(valueArrayField[8] as AnyObject, forKey: "condition")
        data.updateValue(valueArrayField[4] as AnyObject, forKey: "desc")
        data.updateValue(valueArrayField[14] as AnyObject, forKey: "end_stnk")
        data.updateValue(valueArrayField[10] as AnyObject, forKey: "fuel")
        data.updateValue(loginData?.cityId as AnyObject, forKey: "id_kota")
        data.updateValue(idCar as AnyObject, forKey: "id")
        data.updateValue(loginData?.provinceId as AnyObject, forKey: "id_provinsi")
        data.updateValue(valueArrayField[12] as AnyObject, forKey: "kilometer")
        data.updateValue(valueArrayField[16] as AnyObject, forKey: "otomart_id")
        data.updateValue(valueArrayField[5] as AnyObject, forKey: "price")
        data.updateValue(valueArrayField[11] as AnyObject, forKey: "size_cc")
        data.updateValue(valueArrayField[3] as AnyObject, forKey: "special_type")
        data.updateValue(valueArrayField[7] as AnyObject, forKey: "transmition")
        data.updateValue(valueArrayField[6] as AnyObject, forKey: "year")
        data.updateValue(valueArrayField[15] as AnyObject, forKey: "no_polisi")
        
        let image1:Data = images[0].jpeg!
        data.updateValue( image1 as AnyObject, forKey: "picture")
        let image2:Data = images[1].jpeg!
        data.updateValue( image2 as AnyObject, forKey: "picture1")
        let image3:Data = images[2].jpeg!
        data.updateValue( image3 as AnyObject, forKey: "picture2")
        let image4:Data = images[3].jpeg!
        data.updateValue( image4 as AnyObject, forKey: "picture3")
        let image5:Data = images[4].jpeg!
        data.updateValue( image5 as AnyObject, forKey: "picture4")
        if images[5] != UIImage(named: "icon_small_photo") {
            let image6:Data = images[5].jpeg!
            data.updateValue( image6 as AnyObject, forKey: "picture5")
        }
        if images[6] != UIImage(named: "icon_small_photo") {
            let image7:Data = images[6].jpeg!
            data.updateValue( image7 as AnyObject, forKey: "picture6")
        }
        if images[7] != UIImage(named: "icon_small_photo") {
            let image8:Data = images[7].jpeg!
            data.updateValue( image8 as AnyObject, forKey: "picture7")
        }
        if images[8] != UIImage(named: "icon_small_photo") {
            let image9:Data = images[8].jpeg!
            data.updateValue( image9 as AnyObject, forKey: "picture8")
        }
        if images[9] != UIImage(named: "icon_small_photo") {
            let image10:Data = images[9].jpeg!
            data.updateValue( image10 as AnyObject, forKey: "picture9")
        }
        return data
    }
    
    func doUpdateCarList() {
        let data = extractData()
        let result = DataUtility.UpdateCarList(id: carId, data: data)
        if result {
            stopAnimating()
            self.showSuccess(message: "Penyimpanan data berhasil!")
            self.navigationController?.popViewController(animated: true)
        } else {
            stopAnimating()
            self.showError(message: "Gagal menyimpan data Car ke database.")
        }
    }
    
    func doSaveCarList() {
        let data = extractData()
        let result = DataUtility.insertCarData(data: data)
        if result {
            stopAnimating()
            self.showSuccess(message: "Penyimpanan data berhasil!")
            self.navigationController?.popViewController(animated: true)
        } else {
            stopAnimating()
            self.showError(message: "Gagal menyimpan data Car ke database.")
        }
    }
}

extension FormCarViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 19
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell:FormPickerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormPickerTableViewCell") as! FormPickerTableViewCell
            cell.titleField.text = titleArrayField[indexPath.row]
            cell.valueTextField.placeholder = titleArrayField[indexPath.row]
            cell.valueTextField.text = valueArrayField[indexPath.row]
            cell.valueTextField.tag = indexPath.row + 1
            cell.valueTextField.pickerDelegate = self
            cell.valueTextField.type = .strings
            cell.valueTextField.dataSource = self
            cell.valueTextField.isEnabled = true
            cell.notesLabel.isHidden = true
            cell.dropDown.isHidden = false
            return cell
        case 1:
            if modelArr.count > 0 {
                let cell:FormPickerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormPickerTableViewCell") as! FormPickerTableViewCell
                cell.titleField.text = titleArrayField[indexPath.row]
                cell.valueTextField.placeholder = titleArrayField[indexPath.row]
                cell.valueTextField.text = valueArrayField[indexPath.row]
                cell.valueTextField.tag = indexPath.row + 1
                cell.valueTextField.pickerDelegate = self
                cell.valueTextField.type = .strings
                cell.valueTextField.dataSource = self
                cell.valueTextField.isEnabled = true
                cell.notesLabel.isHidden = true
                cell.dropDown.isHidden = false
                return cell
            }else{
                let cell:FormFieldTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormFieldTableViewCell") as! FormFieldTableViewCell
                cell.titleField.text = titleArrayField[indexPath.row]
                cell.valueTextField.placeholder = titleArrayField[indexPath.row]
                cell.valueTextField.text = valueArrayField[indexPath.row]
                cell.valueTextField.delegate = self
                cell.valueTextField.tag = indexPath.row + 1
                cell.valueTextField.inputView = UIView()
                cell.notesLabel.isHidden = true
                cell.dropDown.isHidden = false
                return cell
            }
        case 2:
            if typeArr.count > 0 {
                let cell:FormPickerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormPickerTableViewCell") as! FormPickerTableViewCell
                cell.titleField.text = titleArrayField[indexPath.row]
                cell.valueTextField.placeholder = titleArrayField[indexPath.row]
                cell.valueTextField.text = valueArrayField[indexPath.row]
                cell.valueTextField.tag = indexPath.row + 1
                cell.valueTextField.pickerDelegate = self
                cell.valueTextField.type = .strings
                cell.valueTextField.dataSource = self
                cell.valueTextField.isEnabled = true
                cell.notesLabel.isHidden = true
                cell.dropDown.isHidden = false
                return cell
            }else{
                let cell:FormFieldTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormFieldTableViewCell") as! FormFieldTableViewCell
                cell.titleField.text = titleArrayField[indexPath.row]
                cell.valueTextField.placeholder = titleArrayField[indexPath.row]
                cell.valueTextField.text = valueArrayField[indexPath.row]
                cell.valueTextField.delegate = self
                cell.valueTextField.tag = indexPath.row + 1
                cell.valueTextField.inputView = UIView()
                cell.notesLabel.isHidden = true
                cell.dropDown.isHidden = false
                return cell
            }
        case 3:
            let cell:FormTextTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormTextTableViewCell") as! FormTextTableViewCell
            cell.titleField.text = titleArrayField[indexPath.row]
            cell.valueTextField.placeholder = titleArrayField[indexPath.row]
            cell.valueTextField.text = valueArrayField[indexPath.row]
            cell.valueTextField.delegate = self
            cell.valueTextField.tag = indexPath.row + 1
            cell.notesLabel.isHidden = true
            return cell
        case 4:
            let cell:FormDescTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormDescTableViewCell") as! FormDescTableViewCell
            cell.titleField.text = titleArrayField[indexPath.row]
            cell.descriptionTextView.text = valueArrayField[indexPath.row]
            cell.descriptionTextView.tag = indexPath.row + 1
            cell.descriptionTextView.delegate = self
            return cell
        case 5:
            let cell:FormNumberTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormNumberTableViewCell") as! FormNumberTableViewCell
            cell.titleField.text = titleArrayField[indexPath.row]
            cell.valueTextField.placeholder = titleArrayField[indexPath.row]
            cell.valueTextField.text = valueArrayField[indexPath.row]
            cell.valueTextField.delegate = self
            cell.valueTextField.tag = indexPath.row + 1
            return cell
        case 6:
            let cell:FormPickerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormPickerTableViewCell") as! FormPickerTableViewCell
            cell.titleField.text = titleArrayField[indexPath.row]
            cell.valueTextField.placeholder = titleArrayField[indexPath.row]
            cell.valueTextField.text = valueArrayField[indexPath.row]
            cell.valueTextField.tag = indexPath.row + 1
            cell.valueTextField.pickerDelegate = self
            cell.valueTextField.type = .strings
            cell.valueTextField.dataSource = self
            cell.valueTextField.isEnabled = true
            cell.notesLabel.isHidden = true
            cell.dropDown.isHidden = false
            return cell
        case 7:
            let cell:FormPickerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormPickerTableViewCell") as! FormPickerTableViewCell
            cell.titleField.text = titleArrayField[indexPath.row]
            cell.valueTextField.placeholder = titleArrayField[indexPath.row]
            cell.valueTextField.text = valueArrayField[indexPath.row]
            cell.valueTextField.tag = indexPath.row + 1
            cell.valueTextField.pickerDelegate = self
            cell.valueTextField.type = .strings
            cell.valueTextField.dataSource = self
            cell.valueTextField.isEnabled = true
            cell.notesLabel.isHidden = true
            cell.dropDown.isHidden = false
            return cell
        case 8:
            let cell:FormPickerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormPickerTableViewCell") as! FormPickerTableViewCell
            cell.titleField.text = titleArrayField[indexPath.row]
            cell.valueTextField.placeholder = titleArrayField[indexPath.row]
            cell.valueTextField.text = valueArrayField[indexPath.row]
            cell.valueTextField.tag = indexPath.row + 1
            cell.valueTextField.pickerDelegate = self
            cell.valueTextField.type = .strings
            cell.valueTextField.dataSource = self
            cell.valueTextField.isEnabled = true
            cell.notesLabel.isHidden = true
            cell.dropDown.isHidden = false
            return cell
        case 9:
            let cell:FormPickerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormPickerTableViewCell") as! FormPickerTableViewCell
            cell.titleField.text = titleArrayField[indexPath.row]
            cell.valueTextField.placeholder = titleArrayField[indexPath.row]
            cell.valueTextField.text = valueArrayField[indexPath.row]
            cell.valueTextField.tag = indexPath.row + 1
            cell.valueTextField.isEnabled = true
            cell.valueTextField.pickerDelegate = self
            cell.valueTextField.type = .strings
            cell.valueTextField.dataSource = self
            cell.notesLabel.isHidden = true
            cell.dropDown.isHidden = false
            return cell
        case 10:
            let cell:FormPickerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormPickerTableViewCell") as! FormPickerTableViewCell
            cell.titleField.text = titleArrayField[indexPath.row]
            cell.valueTextField.placeholder = titleArrayField[indexPath.row]
            cell.valueTextField.text = valueArrayField[indexPath.row]
            cell.valueTextField.tag = indexPath.row + 1
            cell.valueTextField.pickerDelegate = self
            cell.valueTextField.type = .strings
            cell.valueTextField.dataSource = self
            cell.valueTextField.isEnabled = true
            cell.notesLabel.isHidden = true
            cell.dropDown.isHidden = false
            return cell
        case 11:
            if typeId == "0" || ccArr.count == 0{
                let cell:FormNumberTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormNumberTableViewCell") as! FormNumberTableViewCell
                cell.titleField.text = titleArrayField[indexPath.row]
                cell.valueTextField.placeholder = titleArrayField[indexPath.row]
                cell.valueTextField.text = valueArrayField[indexPath.row]
                cell.valueTextField.delegate = self
                cell.valueTextField.tag = indexPath.row + 1
                return cell
            }else{
                let cell:FormPickerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormPickerTableViewCell") as! FormPickerTableViewCell
                cell.titleField.text = titleArrayField[indexPath.row]
                cell.valueTextField.placeholder = titleArrayField[indexPath.row]
                cell.valueTextField.text = valueArrayField[indexPath.row]
                cell.valueTextField.tag = indexPath.row + 1
                cell.valueTextField.pickerDelegate = self
                cell.valueTextField.type = .strings
                cell.valueTextField.dataSource = self
                cell.valueTextField.isEnabled = true
                cell.notesLabel.isHidden = true
                cell.dropDown.isHidden = false
                return cell
            }
        case 12:
            let cell:FormNumberTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormNumberTableViewCell") as! FormNumberTableViewCell
            cell.titleField.text = titleArrayField[indexPath.row]
            cell.valueTextField.placeholder = titleArrayField[indexPath.row]
            cell.valueTextField.text = valueArrayField[indexPath.row]
            cell.valueTextField.delegate = self
            cell.valueTextField.tag = indexPath.row + 1
            return cell
        case 13:
            let cell:FormSwitchTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormSwitchTableViewCell") as! FormSwitchTableViewCell
            cell.titleField.text = titleArrayField[indexPath.row]
            cell.delegate = self
            if valueArrayField[indexPath.row] == "false" {
                cell.switchField.isOn = false
            }else{
                cell.switchField.isOn = true
            }
            return cell
        case 14:
            let cell:FormPickerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormPickerTableViewCell") as! FormPickerTableViewCell
            cell.titleField.text = titleArrayField[indexPath.row]
            cell.valueTextField.placeholder = titleArrayField[indexPath.row]
            cell.valueTextField.text = valueArrayField[indexPath.row]
            cell.valueTextField.tag = indexPath.row + 1
            cell.valueTextField.type = .date
            cell.valueTextField.dateFormatter.dateFormat = "yyyy-MM-dd"
            cell.valueTextField.pickerDelegate = self
            if valueArrayField[13] == "false" {
                cell.valueTextField.isEnabled = false
            }else{
                cell.valueTextField.isEnabled = true
            }
            cell.notesLabel.isHidden = true
            cell.dropDown.isHidden = false
            return cell
        case 15:
            let cell:FormTextTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormTextTableViewCell") as! FormTextTableViewCell
            cell.titleField.text = titleArrayField[indexPath.row]
            cell.valueTextField.placeholder = titleArrayField[indexPath.row]
            cell.valueTextField.text = valueArrayField[indexPath.row]
            cell.valueTextField.delegate = self
            cell.valueTextField.tag = indexPath.row + 1
            cell.notesLabel.isHidden = true
            return cell
        case 16:
            let cell:FormTextTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormTextTableViewCell") as! FormTextTableViewCell
            cell.titleField.text = titleArrayField[indexPath.row]
            cell.valueTextField.placeholder = titleArrayField[indexPath.row]
            cell.valueTextField.text = valueArrayField[indexPath.row]
            cell.valueTextField.delegate = self
            cell.valueTextField.tag = indexPath.row + 1
            cell.notesLabel.isHidden = false
            return cell
        case 17:
            let cell:FormPictureTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormPictureTableViewCell") as! FormPictureTableViewCell
            cell.formDelegate = self
            cell.frontImage.image = images[0]
            cell.frontButton.isHidden = !deleteImage[0]
            cell.backImage.image = images[1]
            cell.backButton.isHidden = !deleteImage[1]
            cell.rightImage.image = images[2]
            cell.rightButton.isHidden = !deleteImage[2]
            cell.leftImage.image = images[3]
            cell.leftButton.isHidden = !deleteImage[3]
            cell.interiorImage.image = images[4]
            cell.interiorButton.isHidden = !deleteImage[4]
            cell.sixImage.image = images[5]
            cell.sixButton.isHidden = !deleteImage[5]
            cell.sevenImage.image = images[6]
            cell.sevenButton.isHidden = !deleteImage[6]
            cell.eightImage.image = images[7]
            cell.eightButton.isHidden = !deleteImage[7]
            cell.nineImage.image = images[8]
            cell.nineButton.isHidden = !deleteImage[8]
            cell.tenImage.image = images[9]
            cell.tenButton.isHidden = !deleteImage[9]
            return cell
        case 18:
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell")!
            return cell
        default:
            let cell:FormTextTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FormTextTableViewCell") as! FormTextTableViewCell
            cell.titleField.text = titleArrayField[indexPath.row]
            cell.valueTextField.placeholder = titleArrayField[indexPath.row]
            cell.valueTextField.text = valueArrayField[indexPath.row]
            cell.valueTextField.delegate = self
            cell.valueTextField.tag = indexPath.row + 1
            cell.notesLabel.isHidden = true
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 4:
            return 150.0
        case 13:
            return 50.0
        case 16:
            return 100.0
        case 17:
            return 210.0
        case 18:
            return 100.0
        default:
            return 80.0
        }
    }
}

extension FormCarViewController: APJTextPickerViewDelegate, APJTextPickerViewDataSource {
    func textPickerView(_ textPickerView: APJTextPickerView, didSelectDate date: Date?) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateString = dateFormatter.string(from:date!)
        valueArrayField[14] = dateString
        self.view.setNeedsLayout()
    }
    
    func textPickerView(_ textPickerView: APJTextPickerView, didSelectString row: Int) {
        switch textPickerView.tag {
        case 1:
            valueArrayField[0] = merkArr[row].name!
            if let id = merkArr[row].merk_id {
                merkId = id
                modelArr = DataUtility.fetchModelByMerk(merkId: merkId)
                if merkId != "0" {
                    let tmpModelArr = DataUtility.fetchModelByMerk(merkId: "0")
                    modelArr.append(contentsOf: tmpModelArr)
                }
            }
            self.tableView.reloadData()
        case 2:
            valueArrayField[1] = modelArr[row].name!
            if let id = modelArr[row].id {
                modelId = id
                typeArr = DataUtility.fetchTypeByModel(modelId: modelId)
                if modelId != "0" {
                    let tmpTypeArr = DataUtility.fetchTypeByModel(modelId: "0")
                    typeArr.append(contentsOf: tmpTypeArr)
                }
            }
            self.tableView.reloadData()
        case 3:
            valueArrayField[2] = typeArr[row].name!
            if let id = typeArr[row].id {
                typeId = id
                if typeId == "0" {
                    ccArr = []
                }else{
                    ccArr = DataUtility.fetchCCByTypeId(id: typeId)
                }
            }
            valueArrayField[11] = ""
            self.tableView.reloadData()
        case 7:
            valueArrayField[6] = yearArr[row]
        case 8:
            valueArrayField[7] = transmisi[row]
        case 9:
            valueArrayField[8] = kondisi[row]
        case 10:
            valueArrayField[9] = colorArr[row].name!
            colorId = colorArr[row].id!
        case 11:
            valueArrayField[10] = bahanBakar[row]
        case 12:
            valueArrayField[11] = ccArr[row].cc!
            ccId = ccArr[row].id_type!
        default:
            break
        }
        self.view.setNeedsLayout()
    }
    
    func textPickerView(_ textPickerView: APJTextPickerView, titleForRow row: Int) -> String? {
        self.view.setNeedsLayout()
        switch textPickerView.tag {
        case 1:
            return merkArr[row].name!
        case 2:
            return modelArr[row].name
        case 3:
            return typeArr[row].name
        case 7:
            return yearArr[row]
        case 8:
            return transmisi[row]
        case 9:
            return kondisi[row]
        case 10:
            return colorArr[row].name!
        case 11:
            return bahanBakar[row]
        case 12:
            return ccArr[row].cc
        default:
            return ""
        }
    }
    
    func numberOfRows(in pickerView: APJTextPickerView) -> Int {
        switch pickerView.tag {
        case 1:
            return merkArr.count
        case 2:
            if modelArr.count == 0 {
                showError(message: "Pilih merk terlebih dahulu.")
                pickerView.pickerDelegate?.textPickerCancel()
            }
            return modelArr.count
        case 3:
            if typeArr.count == 0 {
                showError(message: "Pilih model terlebih dahulu.")
                pickerView.pickerDelegate?.textPickerCancel()
            }
            return typeArr.count
        case 7:
            return yearArr.count
        case 8:
            return transmisi.count
        case 9:
            return kondisi.count
        case 10:
            return colorArr.count
        case 11:
            return bahanBakar.count
        case 12:
            if typeId != "0" && ccArr.count == 0 {
                showError(message: "Pilih tipe terlebih dahulu.")
                pickerView.pickerDelegate?.textPickerCancel()
            }
            return ccArr.count
        default:
            return 0
        }
    }
    
    func textPickerCancel() {
        self.view.setNeedsLayout()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension FormCarViewController: UITextFieldDelegate, UITextViewDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 2 {
            if modelArr.count == 0 {
                self.showError(message: "Silahkan pilih merk terlebih dahulu.")
            }
        }
        else if textField.tag == 3 {
            if typeArr.count == 0 {
                self.showError(message: "Silahkan pilih model terlebih dahulu.")
            }
        }
        else if textField.tag == 11 {
            if typeArr.count == 0 {
                self.showError(message: "Silahkan pilih tipe terlebih dahulu.")
            }
        }
        else {
            DispatchQueue.main.async {
                textField.becomeFirstResponder()
            }
        }
        self.view.setNeedsLayout()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 16 {
            valueArrayField[textField.tag-1] = textField.text!.uppercased()
        }else{
            valueArrayField[textField.tag-1] = textField.text!
        }
        self.view.setNeedsLayout()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        valueArrayField[textView.tag-1] = textView.text!
        self.view.setNeedsLayout()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text: NSString = (textField.text ?? "") as NSString
        var finalString = text.replacingCharacters(in: range, with: string)
        if textField.tag == 6 {
            finalString = finalString.replacingOccurrences(of: ".", with: "")
            if finalString == "Rp" {
                textField.text = ""
                valueArrayField[textField.tag-1] = ""
            }else{
                finalString = finalString.replacingOccurrences(of: "Rp", with: "")
                textField.text = "Rp\(Int(finalString)?.formattedWithSeparator ?? "")"
                valueArrayField[textField.tag-1] = "Rp\(Int(finalString)?.formattedWithSeparator ?? "")"
            }
            return false
        }
        if textField.tag == 16 {
            valueArrayField[textField.tag-1] = finalString.uppercased()
        }else{
            valueArrayField[textField.tag-1] = finalString
        }
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let txt: NSString = (textView.text ?? "") as NSString
        let finalString = txt.replacingCharacters(in: range, with: text)
        valueArrayField[textView.tag-1] = finalString
        return true
    }
}

extension FormCarViewController: FormTapImageDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func returnPrevImage(index: Int) {
        images[index-1] = UIImage(named:"icon_small_photo")!
        deleteImage[index-1] = false
        self.tableView.reloadData()
    }
    func returnTapImage1() {
        openDialog(imgPicker:imagePicker1)
    }
    
    func returnTapImage2() {
        openDialog(imgPicker:imagePicker2)
    }
    
    func returnTapImage3() {
        openDialog(imgPicker:imagePicker3)
    }
    
    func returnTapImage4() {
        openDialog(imgPicker:imagePicker4)
    }
    
    func returnTapImage5() {
        openDialog(imgPicker:imagePicker5)
    }
    
    func returnTapImage6() {
        if self.checkingUploadImage() {
            openDialog(imgPicker: imagePicker6)
        }else{
            self.showError(message: self.optionPictMessage)
        }
    }
    
    func returnTapImage7() {
        if self.checkingUploadImage() {
            openDialog(imgPicker: imagePicker7)
        }else{
            self.showError(message: self.optionPictMessage)
        }
    }
    
    func returnTapImage8() {
        if self.checkingUploadImage() {
            openDialog(imgPicker: imagePicker8)
        }else{
            self.showError(message: self.optionPictMessage)
        }
    }
    
    func returnTapImage9() {
        if self.checkingUploadImage() {
            openDialog(imgPicker: imagePicker9)
        }else{
            self.showError(message: self.optionPictMessage)
        }
    }
    
    func returnTapImage10() {
        if self.checkingUploadImage() {
            openDialog(imgPicker: imagePicker10)
        }else{
            self.showError(message: self.optionPictMessage)
        }
    }
    
    func checkingUploadImage() -> Bool {
        for i in 0...4 {
            if images[i] != UIImage(named: "icon_small_photo") {
                return false
            }
        }
        return true
    }
    
    func openDialog(imgPicker:UIImagePickerController) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera(imgPicker:imgPicker)
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery(imgPicker:imgPicker)
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func openGallery(imgPicker:UIImagePickerController) {
        imgPicker.delegate = self
        imgPicker.allowsEditing = false
        imgPicker.sourceType = .photoLibrary
        
        UINavigationBar.appearance().tintColor = UIColor.black
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.black]
        
        present(imgPicker, animated: true, completion: nil)
    }
    
    func openCamera(imgPicker:UIImagePickerController)
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imgPicker.sourceType = UIImagePickerControllerSourceType.camera
            imgPicker.allowsEditing = true
            imgPicker.delegate = self
            
            UINavigationBar.appearance().tintColor = UIColor.black
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.black]
            
            present(imgPicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            UINavigationBar.appearance().tintColor = UIColor.black
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.black]
            
            present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if picker == imagePicker1 {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                images[0] = pickedImage
                deleteImage[0] = true
            }
        }
        if picker == imagePicker2 {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                images[1] = pickedImage
                deleteImage[1] = true
            }
        }
        if picker == imagePicker3 {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                images[2] = pickedImage
                deleteImage[2] = true
            }
        }
        if picker == imagePicker4 {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                images[3] = pickedImage
                deleteImage[3] = true
            }
        }
        if picker == imagePicker5 {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                images[4] = pickedImage
                deleteImage[4] = true
            }
        }
        if picker == imagePicker6 {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                images[5] = pickedImage
                deleteImage[5] = true
            }
        }
        if picker == imagePicker7 {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                images[6] = pickedImage
                deleteImage[6] = true
            }
        }
        if picker == imagePicker8 {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                images[7] = pickedImage
                deleteImage[7] = true
            }
        }
        if picker == imagePicker9 {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                images[8] = pickedImage
                deleteImage[8] = true
            }
        }
        if picker == imagePicker10 {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                images[9] = pickedImage
                deleteImage[9] = true
            }
        }
        dismiss(animated: true, completion: nil)
        self.tableView.reloadData()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension FormCarViewController: FormSwitchDelegate {
    func returnSwitchValue(result: String) {
        valueArrayField[13] = result
        self.tableView.reloadData()
    }
}

extension UIImage {
    var jpeg: Data? {
        return UIImageJPEGRepresentation(self, 0.5)   // QUALITY min = 0 / max = 1
    }
}

extension String {
    var currency: String {
        // removing all characters from string before formatting
        let stringWithoutSymbol = self.replacingOccurrences(of: "Rp", with: "")
        let stringWithoutComma = stringWithoutSymbol.replacingOccurrences(of: ",", with: "")
        
        let styler = NumberFormatter()
        styler.minimumFractionDigits = 0
        styler.maximumFractionDigits = 0
        styler.currencySymbol = "Rp"
        styler.numberStyle = .currency
        
        if let result = NumberFormatter().number(from: stringWithoutComma) {
            return styler.string(from: result)!
        }
        
        return self
    }
}

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension BinaryInteger {
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
}

