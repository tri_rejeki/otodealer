//
//  Login.swift
//  CarManagement
//
//  Created by Tri Rejeki on 01/12/17.
//  Copyright © 2017 OTOmart: Cara Mudah Cari Mobil. All rights reserved.
//

import UIKit

class Login: NSObject, NSCoding {
    
    var cityId: String = ""
    var address: String = ""
    var dealerNew: String = ""
    var dealerUsed: String = ""
    var desc: String = ""
    var disctrictId: String = ""
    var email: String = ""
    var fax: String = ""
    var id: String = ""
    var idListing: String = ""
    var latlong: String = ""
    var name: String = ""
    var phone: String = ""
    var phone1: String = ""
    var phone2: String = ""
    var picture: String = ""
    var provinceId: String = ""
    var suspended: String = ""
    var url: String = ""
    var password: String = ""
    
    init(cityId: String, address: String, dealerNew: String, dealerUsed: String, desc: String, disctrictId: String, email:String, fax: String, id: String, idListing: String, name:String, phone:String, phone1:String, phone2:String, picture:String, provinceId: String, suspended:String, url:String, latlong:String, password:String) {
        self.cityId = cityId
        self.address = address
        self.dealerNew = dealerNew
        self.dealerUsed = dealerUsed
        self.desc = desc
        self.disctrictId = disctrictId
        self.fax = fax
        self.id = id
        self.idListing = idListing
        self.email = email
        self.name = name
        self.phone = phone
        self.phone1 = phone1
        self.phone2 = phone2
        self.picture = picture
        self.provinceId = provinceId
        self.suspended = suspended
        self.url = url
        self.latlong = latlong
        self.password = password
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let cityId = aDecoder.decodeObject(forKey: "cityId") as! String
        let address = aDecoder.decodeObject(forKey: "address") as! String
        let dealerNew = aDecoder.decodeObject(forKey: "dealerNew") as! String
        let dealerUsed = aDecoder.decodeObject(forKey: "dealerUsed") as! String
        let desc = aDecoder.decodeObject(forKey: "desc") as! String
        let disctrictId = aDecoder.decodeObject(forKey: "disctrictId") as! String
        let id = aDecoder.decodeObject(forKey: "id") as! String
        let idListing = aDecoder.decodeObject(forKey: "idListing") as! String
        let fax = aDecoder.decodeObject(forKey: "fax") as! String
        let email = aDecoder.decodeObject(forKey: "email") as! String
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let phone = aDecoder.decodeObject(forKey: "phone") as! String
        let phone1 = aDecoder.decodeObject(forKey: "phone1") as! String
        let phone2 = aDecoder.decodeObject(forKey: "phone2") as! String
        let picture = aDecoder.decodeObject(forKey: "picture") as! String
        let provinceId = aDecoder.decodeObject(forKey: "provinceId") as! String
        let suspended = aDecoder.decodeObject(forKey: "suspended") as! String
        let url = aDecoder.decodeObject(forKey: "url") as! String
        let latlong = aDecoder.decodeObject(forKey: "latlong") as! String
        let password = aDecoder.decodeObject(forKey: "password") as! String
        self.init(cityId: cityId, address: address, dealerNew: dealerNew, dealerUsed: dealerUsed, desc: desc, disctrictId: disctrictId, email: email, fax: fax, id: id, idListing: idListing, name: name, phone: phone, phone1: phone1, phone2: phone2, picture: picture, provinceId: provinceId, suspended: suspended, url: url, latlong: latlong, password: password)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(cityId, forKey: "cityId")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(dealerNew, forKey: "dealerNew")
        aCoder.encode(dealerUsed, forKey: "dealerUsed")
        aCoder.encode(desc, forKey: "desc")
        aCoder.encode(disctrictId, forKey: "disctrictId")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(phone, forKey: "phone")
        aCoder.encode(phone1, forKey: "phone1")
        aCoder.encode(phone2, forKey: "phone2")
        aCoder.encode(picture, forKey: "picture")
        aCoder.encode(provinceId, forKey: "provinceId")
        aCoder.encode(suspended, forKey: "suspended")
        aCoder.encode(url, forKey: "url")
        aCoder.encode(id, forKey: "id")
        aCoder.encode(idListing, forKey: "idListing")
        aCoder.encode(fax, forKey: "fax")
        aCoder.encode(latlong, forKey: "latlong")
        aCoder.encode(password, forKey: "password")
    }
}
